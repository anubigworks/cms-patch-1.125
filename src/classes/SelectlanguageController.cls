public without sharing class SelectlanguageController {
	public Boolean display { get; set; }
	public String selectLanguage { get; set; }
	public List<SelectOption> languages {
		get {
			return new List<SelectOption> {
				new SelectOption('zh_CN', 'Chinese (Simplified)'),
				new SelectOption('zh_TW', 'Chinese (Traditional)'),
				new SelectOption('da', 'Danish'),
				new SelectOption('nl_NL', 'Dutch'),
				new SelectOption('en_US', 'English'),
				new SelectOption('fi', 'Finnish'),
				new SelectOption('fr', 'French'),
				new SelectOption('de', 'German'),
				new SelectOption('it', 'Italian'),
				new SelectOption('ja', 'Japanese'),
				new SelectOption('ko', 'Korean'),
				new SelectOption('pt_BR', 'Portuguese (Brazil)'),
				new SelectOption('ru', 'Russian'),
				new SelectOption('es', 'Spanish'),
				new SelectOption('sv', 'Swedish'),
				new SelectOption('th', 'Thai')
			};
		}

		private set;
	}

	public PageReference init() {
		
		return null;
	}

	public PageReference changeLanguage() {
		
		return null;
	}
}