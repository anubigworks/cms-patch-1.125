/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LinkedinControllerTest {

	static testMethod void myUnitTest() {
		// TO DO: implement unit test
		ApexPages.currentPage().getParameters().put('code', 'AQQVbPlBn2jY1HDgDvBidD4aLfmxmotgRl0qUa8w6w0RuvUJqCUmoZW8xiUTCwnrfXev3GkRQLqlyfhm3mBfSdekjCQefvRcuod-LQY_OuVp5PLa11E');
		ApexPages.currentPage().getParameters().put('state', 'abcde12345');

		CustomSettingsComponent__c linkedInSetting = new CustomSettingsComponent__c(
			Name = 'Default',
			FCMS__LinkedIn_Redirect_Uri__c = 'https://fcms-developer-edition.ap1.force.com/',
			FCMS__LinkedIn_Client_Id__c = '75n4vp24esl36t' ,
			FCMS__LinkedIn_Client_Secret__c = 'IRh8mNKxg8RmMwDf',
			FCMS__ErrorsReceiver__c = 'rakesh.r@targetrecruit.net'
		);
		insert linkedInSetting;

		Account testAcc = new Account(Name = 'TestAcc');
		insert testAcc;

		Page__c testPage = new Page__c(Name = 'TestPage', Name__c = 'TestPage');

		CMSProfile__c testCMSProfile = new CMSProfile__c( 
			Name = 'TestProfile',
			Need_Registration_Approval__c = true,
			Name__c = 'TestProfile',
			Profile_Account__c = testAcc.id,
			Force_com_Authorization_and_Registration__c = false,
			User_Registration__c = true,
			Default_Home_Page__c = testPage.id,
			order__c = 1);
		insert testCMSProfile;
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResGenerator());

		LinkedInAuthController controller = new LinkedInAuthController();
		controller.linkedInLogin();
	}
}