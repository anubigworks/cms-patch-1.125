public abstract class SocialLoginHelper {

	public virtual PageReference registerSocialUser( Map<String,String> userInfo, String source ) {
	
		List<Contact> contacts = [
			SELECT Registration_Approved__c, CMSProfile__c,
			CMSProfile__r.Name__c,
			CMSProfile__r.Order__c,
			CMSProfile__r.Default_Home_Page__r.Name__c
			FROM Contact c
			WHERE Email = :userInfo.get('email').toLowerCase()
			LIMIT 1];
			
		String jobSiteParam = '';

		if (contacts.size() == 0) {
			Contact cont = new Contact();
			cont.Email = userInfo.get('email');
			cont.UserName__c = userInfo.get('email');
			cont.FirstName = userInfo.get('firstName');
			cont.LastName = userInfo.get('lastName');
			cont.Company_Name__c = encryptString(generatePassword());
			cont.CMSProfile__c = [
				SELECT Id
				FROM CMSProfile__c
				WHERE User_Registration__c = true
				ORDER BY Order__c
				LIMIT 1].Id;

			if(source == 'Google') {
				cont.FCMS__GoogleId__c = userInfo.get('id');
			}
			else if(source == 'Facebook') {
				cont.FCMS__FacebookId__c = userInfo.get('id');
			}
			else if(source == 'LinkedIn') {
				cont.FCMS__LinkedInId__c = userInfo.get('id');
			}
	
			System.debug(':::::::::::::cont = ' + cont);
			//Calling Register user method from CMSSiteRegisterController class
			CMSSiteRegisterController register = new CMSSiteRegisterController();
			Pagereference pg = register.registerUserForContact(cont, true);
			//pg.setRedirect(true);
			
			return pg;
		} else {
			//case #00035067
			if(contacts[0].CMSProfile__r.Order__c != 1) {
				CMSProfile__c  candidateProfile = [
				SELECT Name__c
				FROM CMSProfile__c
				WHERE User_Registration__c = true AND Order__c = 1
				ORDER BY Order__c
				LIMIT 1];
				
				if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
					
					jobSiteParam = ApexPages.currentPage().getParameters().get('jobSite');
				}
				
				String errorMsg = String.format(
							System.Label.Social_Login_User_Exists_different_profile,
							new String[] { candidateProfile.Name__c, jobSiteParam });
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
				ApexPages.addMessage(msg);
				return null;
			}
			else {
				if(source == 'Google') {
					contacts[0].FCMS__GoogleId__c = userInfo.get('id');
				}
				else if(source == 'Facebook') {
					contacts[0].FCMS__FacebookId__c = userInfo.get('id');
				}
				else if(source == 'LinkedIn') {
					contacts[0].FCMS__LinkedInId__c = userInfo.get('id');
				}
	
				update contacts;
	
				return CMSHelper.doLogin(contacts[0], contacts[0].CMSProfile__r.Name__c,
											contacts[0].CMSProfile__r.Default_Home_Page__r.Name__c);
			}
		}

		return null;
	}
	
	private String generatePassword() {
		Integer randomInteger = Crypto.getRandomInteger();
		randomInteger = randomInteger > 0 ? randomInteger : randomInteger * -1;
		return String.valueOf(randomInteger);
	}

	public virtual String postDataNew(String url, String method) {
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();

		req.setEndpoint(url);
		req.setMethod(method);
		req.setCompressed(false);
		req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
		res = http.send(req);
		if(res.getStatusCode() == 200){
			System.debug('...info from server...'+res.getBody());
			return res.getBody();

		}else{
			System.debug('...failed to get info from server...');
			return 'error';
		}

	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}

}