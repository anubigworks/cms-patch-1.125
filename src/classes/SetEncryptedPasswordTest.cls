@IsTest
private class SetEncryptedPasswordTest {
	@IsTest
	private static void test() {
		Contact contact = new Contact(
				LastName = 'Test',
				FCMS__UserName__c = 'un@gmail.com',
				FCMS__Password__c = '1234');
		insert contact;
		
		Test.startTest();
		
		Database.executeBatch(new SetEncryptedPassword());
		
		Test.stopTest();
		
		contact = [
				SELECT FCMS__Password__c, FCMS__Company_Name__c
				FROM Contact
		];
		
		System.assertEquals(null, contact.Password__c);
		System.assertEquals('1234', decryptString(contact.FCMS__Company_Name__c));
	}
	
	private static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
}