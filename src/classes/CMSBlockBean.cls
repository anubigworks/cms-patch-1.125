public without sharing class CMSBlockBean {
		// This Class is created for giving all types of blocks content to regions pages
		public Block__c block { get;set; }
		public String menuDisplayType { get;set; }
		public String menuOverrideClass { get;set; }
		//ulID is used for menu functionality. now we can remove it.
		public String ulID { get;set; }
		public String includePageName { get;set; }
		public Integer blockNumber { get;set; }
		public List<CMSMenuBean> cmsMenuBeanList = new List<CMSMenuBean>();
		
		public static CMSMenuBean setCMSMenuBean(Block__c blk, Menu__c menu, 
														List<Menu__c> childMenuList) {
															
			CMSMenuBean cmsMenuBean = new CMSMenuBean();
			cmsMenuBean.parentMenu = menu;
			
			if (childMenuList != null && childMenuList.size()>0) {
				cmsMenuBean.isChildMenusExist = true;
			}
			
			cmsMenuBean.liID = blk.Section__c+blk.Weight__c+menu.Order__c;
			cmsMenuBean.childMenuList = childMenuList;
			
			return cmsMenuBean;
		}
		
		public List<CMSMenuBean> getCmsMenuBeanList() {
			return cmsMenuBeanList;
		}
		
		public void setCmsMenuBeanList(List<CMSMenuBean> cmsMenuBeanList) {
			this.cmsMenuBeanList = cmsMenuBeanList;
		}
		
		public class CMSMenuBean {
			public Menu__c parentMenu { get;set; }
			public Boolean isChildMenusExist{ get;set; }
			public String liID{ get;set; }
			public List<Menu__c> childMenuList { get;set; }
		}
	}