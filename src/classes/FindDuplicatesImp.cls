global without sharing class FindDuplicatesImp implements FindDuplicates {
	global Boolean hasDuplicates(Contact contact) {
		System.debug('FindDuplicatesImp hasDuplicates');
		List<Contact> oldContactsList = [
				SELECT UserName__c, Email, Company_Name__c
				FROM Contact
				WHERE UserName__c = :contact.UserName__c.toLowerCase()
					AND Company_Name__c != ''];
		
		List<User> oldUserList = [
				SELECT Name, Email
				FROM User
				WHERE Username = :contact.UserName__c.toLowerCase()];
		
		return oldContactsList.size() > 0 || oldUserList.size() > 0;
	}
}