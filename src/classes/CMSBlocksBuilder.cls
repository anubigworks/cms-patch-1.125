public without sharing class CMSBlocksBuilder {
	
	private static Contact contact = CMSHelper.getLoggedInContact();
	private static CMSProfile__c profile = CMSHelper.getProfile();
	
	private static List<Permission__c> profilePermissions = [
			SELECT Block__c, Page__c, Menu__c
			FROM Permission__c 
			WHERE CMSProfile__c = :profile.Id];
	
	private static String pageName = ApexPages.currentPage().getParameters().get('page');
	
	private static Set<Id> pageIds = 
			GroupByHelper.getFieldValuesIds(profilePermissions, 'Page__c');
	
	private static String pageAccessWherePart = '(Id IN :pageIds OR Authentication_Required__c = false)';
	
	private static List<Page__c> pageInList = Database.query(
			' SELECT Name__c, Authentication_Required__c, Active__c ' +
			' FROM Page__c ' +
			' WHERE ' + pageAccessWherePart  +
				' AND Active__c = true ' +
				' AND Name__c = :pageName ');
	
	private static Page__c page = pageInList.size() > 0 ? pageInList[0] : null;
	
	
	private static Set<Id> blockIds = 
			GroupByHelper.getFieldValuesIds(profilePermissions, 'Block__c');
	
	private static String blockAccessWherePart = contact.Id != null 
			? ' Id IN :blockIds ' : 'Id IN :blockIds AND Authentication_Required__c = false';
	
	private static List<Block__c> blocks = Database.query(
			' SELECT Auto_Height_Iframe_Code__c, TextContent__c, ' +
				' Add_Parameters_From_Parent_Window__c, Flash_Play__c, ' +
				' Flash_Loop__c, FlashVars__c, Existing_Block_Name__c, ' +
				' Width__c, Weight__c, Type__c, Section__c, Page__r.Name__c, ' +
				' Override_Class__c, Name__c, Module_Name__c, IFrame_URL__c, Height__c, ' +
				' Content__c, Active__c, Menu_Component_Name__c ' +
			' FROM Block__c ' +
			' WHERE ' + blockAccessWherePart  +
				' AND Active__c = true ' +
				' AND (Page__c = null OR Page__r.Name__c = :pageName) ' +
			' ORDER BY Weight__c ');
	
	private static Map<Object, List<Block__c>> sectionToBlocks = 
			(Map<Object, List<Block__c>>)GroupByHelper.groupByField(blocks, 'Section__c');
	
	
	private static Set<Id> menuIds = 
			GroupByHelper.getFieldValuesIds(profilePermissions, 'Menu__c');
	
	private static String menuAccessWherePart = contact.Id != null 
			? ' Id IN :menuIds ' : 'Id IN :menuIds AND Visiblity_Authentication__c = false';
	
	private static List<Menu__c> menus = Database.query(
			' SELECT Name, Parent_Menu__c, Parameters__c, Page__r.Name__c, ' +
				' Order__c, Name__c, Link__c, Active__c ' +
			' FROM Menu__c ' +
			' WHERE ' + menuAccessWherePart  +
				' AND Active__c = true ' +
			' ORDER BY Order__c ');
	
	private static Map<Object, sObject> menuNameToMenu = 
			(Map<Object, sObject>)GroupByHelper.groupByFieldReturnSingle(menus, 'Name');
	
	private static Map<Object, List<Menu__c>> parentMenuIdTochildMenus = 
			(Map<Object, List<Menu__c>>)GroupByHelper.groupByField(menus, 'Parent_Menu__c');
	
	public static List<CMSBlockBean> cmsBlockBuilderMethod(String section) {
		List<Block__c> sectionBlocks = sectionToBlocks.get(section);
		
		if (page == null || sectionBlocks == null) {
			return new List<CMSBlockBean>();
		}
		
		List<CMSBlockBean> blockBeanList = new List<CMSBlockBean>();
		
		
		List<Permission__c> permissionList = new List<Permission__c>();
		
		for (Block__c blk : sectionBlocks) {
			CMSBlockBean bean = new CMSBlockBean();
			
			if (blk.Type__c == System.Label.IFRAME) {
				String src;
				
				if (blk.IFrame_URL__c.contains('?')) {
					src = '&';
				} else {
					src = '?';
				}
				List<String> parametersList = new List<String>();
				List<String> parametersNameList = new List<String>();
				List<String> parametersValueList = new List<String>();
				
				if (blk.Add_Parameters_From_Parent_Window__c!=null 
							&& blk.Add_Parameters_From_Parent_Window__c !='') {
								
					parametersList=blk.Add_Parameters_From_Parent_Window__c.split(',');
					
					for (String parameter:parametersList) {
						parameter = parameter.trim();
						List<String> parameterNameValueList = parameter.split('=');
						if (parameterNameValueList.size()==2) {
							System.debug('*****ramesh***parameterNameValueList.size()==2***');
							parametersNameList.add(parameterNameValueList[0]);
							parametersValueList.add(parameterNameValueList[1]);
						} else if (parameterNameValueList.size()==1) {
							System.debug('*****ramesh***parameterNameValueList.size()==1***');
							parametersNameList.add(parameterNameValueList[0]);
							parametersValueList.add('');
						}
					}
					
					// Changed by Mikhail 11/23/2010 to remove parameter from iFrame 
					// if value is not presented in parent page
					String additionalParameters = '';
					for (Integer x = 0;x<parametersNameList.size();x++) {
						
						String name = parametersNameList[x];
						String value = parametersValueList[x];
						
						if(value == '') {
							System.debug('*****ramesh***parametersValueList[x]11***');
							String par = ApexPages.currentPage().getParameters().get(name);
							if (par != null)
								additionalParameters += '&' + name + '=' + par;
						}
						else
						{
							System.debug('*****ramesh***parametersValueList[x]22***');
							additionalParameters += '&' + name + '=' + value;
						}
					}
					if (additionalParameters != '')
						additionalParameters = additionalParameters.substring(1);
						
					src += additionalParameters;                
				}
				System.debug('*****ramesh***src***'+src);
				blk.IFrame_URL__c = '<iframe src=\''+blk.IFrame_URL__c+src+'\''+' width=\''+
						blk.Width__c+'\' height=\''+
						blk.Height__c+'\' scrolling=\'auto\' frameborder=\'0\'> </iframe>';
				
				System.debug('****ramesh****blk.IFrame_URL__c*****'+blk.IFrame_URL__c);
			}
			
			
			if(blk.Type__c == System.Label.Auto_Height_Iframe) {
				blk.Auto_Height_Iframe_Code__c = blk.Auto_Height_Iframe_Code__c.replace('&lt;','<');
				blk.Auto_Height_Iframe_Code__c = blk.Auto_Height_Iframe_Code__c.replace('&quot;','"');
				blk.Auto_Height_Iframe_Code__c = blk.Auto_Height_Iframe_Code__c.replace('&gt;','>');
				blk.Auto_Height_Iframe_Code__c = blk.Auto_Height_Iframe_Code__c.replace('&#39;','\'');
				blk.Auto_Height_Iframe_Code__c = blk.Auto_Height_Iframe_Code__c.replace('&amp;','&');
				
				List<String> autoHeightIframeCodeOuterList;
				List<String> autoHeightIframeCodeInnerList;
				if (blk.Auto_Height_Iframe_Code__c!=null && 
						blk.Auto_Height_Iframe_Code__c.length()>0) {
							
					String autoHeightIframeCode;
					autoHeightIframeCodeOuterList = blk.Auto_Height_Iframe_Code__c.split('.load',2);
					
					
					if (autoHeightIframeCodeOuterList!=null && 
							autoHeightIframeCodeOuterList.size()>1) {
						autoHeightIframeCode = autoHeightIframeCodeOuterList[0]+'.load';
						autoHeightIframeCodeInnerList=autoHeightIframeCodeOuterList[1].split(';',2);
						
						if (autoHeightIframeCodeInnerList!=null && 
								autoHeightIframeCodeInnerList.size()>1) {
									
							
							String src;
							
							if (autoHeightIframeCodeInnerList[0].contains('?')) {
								src = '&';
							} else {
								src = '?';
							}
							
							List<String> parametersList = new List<String>();
							List<String> parametersNameList = new List<String>();
							List<String> parametersValueList = new List<String>();
							if (blk.Add_Parameters_From_Parent_Window__c!=null && 
									blk.Add_Parameters_From_Parent_Window__c !='') {
										
								parametersList=blk.Add_Parameters_From_Parent_Window__c.split(',');
								for (String parameter:parametersList) {
									parameter = parameter.trim();
									List<String> parameterNameValueList = parameter.split('=');
									if (parameterNameValueList.size()==2) {
										parametersNameList.add(parameterNameValueList[0]);
										parametersValueList.add(parameterNameValueList[1]);
									} else if (parameterNameValueList.size()==1) {
										parametersNameList.add(parameterNameValueList[0]);
										parametersValueList.add('');
									}
								}
								
								for (Integer x = 0;x<parametersNameList.size();x++) {
									if (x==0) {
										src=src+parametersNameList[x];
										if (parametersValueList[x]=='') {
											src = src+'='+ApexPages.currentPage().getParameters()
																		.get(parametersNameList[x]);
										} else {
											src = src+'='+parametersValueList[x];
										}
									} else {
										
										src=src+'&'+parametersNameList[x];
										if (parametersValueList[x]=='') {
											src = src+'='+ApexPages.currentPage().getParameters().
																		get(parametersNameList[x]);
										} else {
											src = src+'='+parametersValueList[x];
										}
									}
								}
								
								System.debug('');
								System.debug('');
							}
							autoHeightIframeCodeInnerList[0]=autoHeightIframeCodeInnerList[0]
																				.replace('")','');
							
							if (src!=null && src.length()>1) {
								autoHeightIframeCodeInnerList[0]=autoHeightIframeCodeInnerList[0]+src;
							}
							
							autoHeightIframeCodeInnerList[0]=autoHeightIframeCodeInnerList[0]+'")';
							System.debug('************autoHeightIframeCodeInnerList[0]******'+
											autoHeightIframeCodeInnerList[0]);
											
							autoHeightIframeCode=autoHeightIframeCode+
									autoHeightIframeCodeInnerList[0]+';'
									+autoHeightIframeCodeInnerList[1];
									
							blk.Auto_Height_Iframe_Code__c=autoHeightIframeCode;
							
						}
					}
				
				}
				
				System.debug('******Ramesh Text******blk.TextContent__c******'+blk.TextContent__c);
				bean.block = blk;
			}
			
			if (blk.Type__c == 'Menu Component') {
				List<Menu_Component__c> menuComponentInList = [
						SELECT Override_Class__c, Name__c, Display_Type__c, 
							Component_Menus__c, Active__c 
						FROM Menu_Component__c 
						WHERE Name__c = :blk.Menu_Component_Name__c];
				
				Menu_Component__c menuComponent = 
						menuComponentInList.size() > 0 ? menuComponentInList[0] : null;
				
				if (menuComponent != null && String.isNotBlank(menuComponent.Component_Menus__c)) {
					List<Menu__c> blockMenus = new List<Menu__c>();
					for (String menuName : menuComponent.Component_Menus__c.split(',')) {
						Menu__c parentMenu = (Menu__c)menuNameToMenu.get(menuName);
						if (parentMenu != null) {
							List<Menu__c> childMenus = parentMenuIdTochildMenus.get(parentMenu.Id);
							
							bean.cmsMenuBeanList.add(
									CMSBlockBean.setCMSMenuBean(blk, parentMenu, childMenus));
						}
					}
					
					bean.ulID = blk.Section__c + blk.Weight__c;
					bean.menuDisplayType = menuComponent.Display_Type__c;
					bean.menuOverrideClass = menuComponent.Override_Class__c; 
				}
			}
			if (blk.Type__c == System.Label.MODULE) {
				bean.includePageName = blk.Module_Name__c;
			}
			if (blk.Type__c=='Text') {
				if (blk.TextContent__c!=null) {
					
					blk.TextContent__c = blk.TextContent__c.replace('&lt;','<');
					blk.TextContent__c = blk.TextContent__c.replace('&quot;','"');
					blk.TextContent__c = blk.TextContent__c.replace('&gt;','>');
					blk.TextContent__c = blk.TextContent__c.replace('&#39;','\'');
					blk.TextContent__c = blk.TextContent__c.replace('&amp;','&');
					
					System.debug('******blk.TextContent__c******'+blk.TextContent__c);
				}
				bean.block = blk;
			}
			
			//Below code is for if block type is existing block.
			if (blk.Type__c=='Existing Block') {
				
				List<Block__c> existBlkList = new List<Block__c>();
				existBlkList.add(blk);
				List<Block__c> existingBlockList = 
											getExistingBlock(existBlkList);
											
				
				System.debug
						('******ramesh***public static List<Block__c> getExistingBlock(********'
						+existingBlockList);
				
				if (existingBlockList.size()>0) {
					if (existingBlockList[0].Type__c==System.Label.IFRAME) {
						String src;
						if (existingBlockList[0].IFrame_URL__c.contains('?')) {
							src = '&';
						} else {
							src = '?';
						}
						List<String> parametersList = new List<String>();
						List<String> parametersNameList = new List<String>();
						List<String> parametersValueList = new List<String>();
						
						if(existingBlockList[0].Add_Parameters_From_Parent_Window__c!=null 
								&& existingBlockList[0].Add_Parameters_From_Parent_Window__c !='') {
							
							parametersList=
								existingBlockList[0].Add_Parameters_From_Parent_Window__c.split(',');
							
							for (String parameter:parametersList) {
								parameter = parameter.trim();
								List<String> parameterNameValueList = parameter.split('=');
								
								if (parameterNameValueList.size()==2) {
									parametersNameList.add(parameterNameValueList[0]);
									parametersValueList.add(parameterNameValueList[1]);
								} else if (parameterNameValueList.size()==1) {
									
									parametersNameList.add(parameterNameValueList[0]);
									parametersValueList.add('');
								}
							}
							for (Integer x = 0;x<parametersNameList.size();x++) {
								
								if (x==0) {
									src=src+parametersNameList[x];
									if (parametersValueList[x]=='') {
										src = src+'='+
										ApexPages.currentPage().getParameters().get
										(parametersNameList[x]);
									} else {
										src = src+'='+parametersValueList[x];
									}
								} else {
									src=src+'&'+parametersNameList[x];
									
									if (parametersValueList[x]=='') {
										src = src+'='+ApexPages.currentPage().getParameters()
																		.get(parametersNameList[x]);
									} else {
										src = src+'='+parametersValueList[x];
									}
								}
							}
						}
						System.debug('*****ramesh***src***'+src);
						blk.IFrame_URL__c = '<iframe src=\''+existingBlockList[0].IFrame_URL__c+
						src+'\''+' width=\''+existingBlockList[0].Width__c+'\' height=\''
						+existingBlockList[0].Height__c+
						'\' scrolling=\'auto\' frameborder=\'0\'> </iframe>';
						
						System.debug('****ramesh****blk.IFrame_URL__c*****'+blk.IFrame_URL__c);
					}
					if (existingBlockList[0].Type__c == System.Label.Text) {
						if (existingBlockList[0].TextContent__c!=null) {
							
							existingBlockList[0].TextContent__c = 
									existingBlockList[0].TextContent__c.replace('&lt;','<');
							
							existingBlockList[0].TextContent__c = 
									existingBlockList[0].TextContent__c.replace('&quot;','"');
							
							existingBlockList[0].TextContent__c = 
									existingBlockList[0].TextContent__c.replace('&gt;','>');
							existingBlockList[0].TextContent__c = 
									existingBlockList[0].TextContent__c.replace('&#39;','\'');
							existingBlockList[0].TextContent__c = 
									existingBlockList[0].TextContent__c.replace('&amp;','&');
						}
					}
					
					if (existingBlockList[0].Type__c == System.Label.Auto_Height_Iframe) {
						
						existingBlockList[0].Auto_Height_Iframe_Code__c = 
								existingBlockList[0].Auto_Height_Iframe_Code__c.replace('&lt;','<');
						
						existingBlockList[0].Auto_Height_Iframe_Code__c = 
								existingBlockList[0].Auto_Height_Iframe_Code__c.replace('&quot;','"');
						
						existingBlockList[0].Auto_Height_Iframe_Code__c = 
								existingBlockList[0].Auto_Height_Iframe_Code__c.replace('&gt;','>');
						
						existingBlockList[0].Auto_Height_Iframe_Code__c = 
								existingBlockList[0].Auto_Height_Iframe_Code__c.replace('&#39;','\'');
						
						existingBlockList[0].Auto_Height_Iframe_Code__c = 
								existingBlockList[0].Auto_Height_Iframe_Code__c.replace('&amp;','&');
						
						List<String> autoHeightIframeCodeOuterList;
						List<String> autoHeightIframeCodeInnerList;
						
						if (existingBlockList[0].Auto_Height_Iframe_Code__c!=null 
								&& existingBlockList[0].Auto_Height_Iframe_Code__c.length()>0) {
									
							String autoHeightIframeCode;
							autoHeightIframeCodeOuterList = 
									existingBlockList[0].Auto_Height_Iframe_Code__c.split('.load',2);
							
							if (autoHeightIframeCodeOuterList!=null 
									&& autoHeightIframeCodeOuterList.size()>1) {
										
								autoHeightIframeCode = autoHeightIframeCodeOuterList[0]+'.load';
								autoHeightIframeCodeInnerList=
													autoHeightIframeCodeOuterList[1].split(';',2);
								
								if (autoHeightIframeCodeInnerList!=null 
														&& autoHeightIframeCodeInnerList.size()>1){
								
									String src;
									if (autoHeightIframeCodeInnerList[0].contains('?')) {
										src = '&';
									} else {
										src = '?';
									}
									
									List<String> parametersList = new List<String>();
									List<String> parametersNameList = new List<String>();
									List<String> parametersValueList = new List<String>();
									if (existingBlockList[0].Add_Parameters_From_Parent_Window__c!=null 
											&& existingBlockList[0].
														Add_Parameters_From_Parent_Window__c !='') {
										parametersList=existingBlockList[0].
													Add_Parameters_From_Parent_Window__c.split(',');
										
										for(String parameter:parametersList) {
											parameter = parameter.trim();
											List<String> parameterNameValueList = parameter.split('=');
											
											if (parameterNameValueList.size()==2) {
												parametersNameList.add(parameterNameValueList[0]);
												parametersValueList.add(parameterNameValueList[1]);
											}
											else if(parameterNameValueList.size()==1){
												parametersNameList.add(parameterNameValueList[0]);
												parametersValueList.add('');
											}
										}
										for(Integer x = 0;x<parametersNameList.size();x++){
											if(x==0){
												src=src+parametersNameList[x];
												if(parametersValueList[x]==''){
													src = src+'='+ApexPages.currentPage().
														getParameters().get(parametersNameList[x]);
												} else {
													src = src+'='+parametersValueList[x];
												}
											} else {
												src=src+'&'+parametersNameList[x];
												if (parametersValueList[x]=='') {
													src = src+'='+ApexPages.currentPage()
														.getParameters().get(parametersNameList[x]);
												} else {
													src = src+'='+parametersValueList[x];
												}
											}
										}
										
										System.debug('');
										System.debug('');
									}
									autoHeightIframeCodeInnerList[0]=
											autoHeightIframeCodeInnerList[0].replace('")','');
									
									if (src!=null && src.length()>1) {
										autoHeightIframeCodeInnerList[0]=
												autoHeightIframeCodeInnerList[0]+src;
									}
									
									autoHeightIframeCodeInnerList[0]=
											autoHeightIframeCodeInnerList[0]+'")';
								
									autoHeightIframeCode=
											autoHeightIframeCode+autoHeightIframeCodeInnerList[0]+
											';'+autoHeightIframeCodeInnerList[1];
											
									existingBlockList[0].Auto_Height_Iframe_Code__c = 
											autoHeightIframeCode;
									
								}
							}
						
						}
						
						
					}
				
					if (existingBlockList[0].Type__c == System.Label.Menu_Component) {
						List<Menu_Component__c> menuComponentInList = [
								SELECT Override_Class__c, Name__c, Display_Type__c, 
									Component_Menus__c, Active__c 
								FROM Menu_Component__c 
								WHERE Name__c = :existingBlockList[0].Menu_Component_Name__c];
						
						Menu_Component__c menuComponent = 
								menuComponentInList.size() > 0 ? menuComponentInList[0] : null;
						
						if (menuComponent != null && String.isNotBlank(menuComponent.Component_Menus__c)) {
							List<Menu__c> blockMenus = new List<Menu__c>();
							for (String menuName : menuComponent.Component_Menus__c.split(',')) {
								Menu__c parentMenu = (Menu__c)menuNameToMenu.get(menuName);
								if (parentMenu != null) {
									List<Menu__c> childMenus = parentMenuIdTochildMenus.get(parentMenu.Id);
									
									bean.cmsMenuBeanList.add(
											CMSBlockBean.setCMSMenuBean(blk, parentMenu, childMenus));
								}
							}
							
							bean.ulID = blk.Section__c + blk.Weight__c;
							bean.menuDisplayType = menuComponent.Display_Type__c;
							bean.menuOverrideClass = menuComponent.Override_Class__c; 
						}
					}
					
					if (existingBlockList[0].Type__c == System.Label.MODULE) {
						bean.includePageName = existingBlockList[0].Module_Name__c;
					}
					
					
					if (existingBlockList[0].Type__c == System.Label.Flash) {
						blk.IFrame_URL__c = existingBlockList[0].IFrame_URL__c;
					}
					blk.Type__c = existingBlockList[0].Type__c;
					blk.Flash_Play__c = existingBlockList[0].Flash_Play__c;
					blk.Flash_Loop__c = existingBlockList[0].Flash_Loop__c;
					blk.FlashVars__c = existingBlockList[0].FlashVars__c;
					blk.Override_Class__c = existingBlockList[0].Override_Class__c;
					blk.Module_Name__c = existingBlockList[0].Module_Name__c;
					blk.Width__c = existingBlockList[0].Width__c;
					blk.Height__c = existingBlockList[0].Height__c;
					blk.Content__c = existingBlockList[0].Content__c;
					blk.Auto_Height_Iframe_Code__c=existingBlockList[0].Auto_Height_Iframe_Code__c;
					blk.TextContent__c=existingBlockList[0].TextContent__c;
					bean.block = blk;
				} else {
					bean.block = blk;
				}
			} else {
				bean.block = blk;
			}
			
			blockBeanList.add(bean);
		}
		
		for (Integer i = 0; i < blockBeanList.size(); i++) {
			blockBeanList[i].blockNumber = i;
		}
		
		return blockBeanList;
	}
	
	//This static method will return the block information for block which block type is existing block in any number of deep node.
	public static List<Block__c> getExistingBlock(List<Block__c> existingBlockList) {
		List<Block__c> existingBlockList1;
		
		List<Permission__c> blockPermissionList = [
				SELECT id,p.Block__r.id 
				FROM Permission__c p 
				WHERE p.CMSProfile__c = :profile.id 
				AND p.Block__r.Name__c = :existingBlockList[0].Existing_Block_Name__c];
				
		String blockId;
		if (blockPermissionList.size()>0) {
			blockId = blockPermissionList[0].Block__r.id;
		}
		
		if (contact.Id!=null) {
			existingBlockList1 = [
					SELECT b.Auto_Height_Iframe_Code__c,b.TextContent__c,
					b.Add_Parameters_From_Parent_Window__c,b.Flash_Play__c,b.Flash_Loop__c,
					b.FlashVars__c,b.Existing_Block_Name__c,b.Width__c,b.Weight__c, 
					b.Type__c, b.Section__c, b.Page__r.Name__c, b.Page__c,b.Override_Class__c, 
					b.Name__c, b.Module_Name__c, b.Id, b.IFrame_URL__c, b.Height__c, 
					b.Content__c, b.Active__c, b.Menu_Component_Name__c 
					FROM Block__c b 
					WHERE b.id = :blockId 
					AND b.Active__c=true 
					AND  b.Name__c =:existingBlockList[0].Existing_Block_Name__c 
					LIMIT 1];
		} else {
			existingBlockList1 = [
						SELECT b.Auto_Height_Iframe_Code__c,b.TextContent__c,
						b.Add_Parameters_From_Parent_Window__c, 
						b.Flash_Play__c,b.Flash_Loop__c,b.FlashVars__c,
						b.Existing_Block_Name__c,b.Width__c, b.Weight__c, 
						b.Type__c,b.Section__c, b.Page__r.Name__c, b.Page__c, 
						b.Override_Class__c, b.Name__c, b.Module_Name__c, 
						b.Id, b.IFrame_URL__c, b.Height__c, 
						b.Content__c, b.Active__c, b.Menu_Component_Name__c 
						FROM Block__c b 
						WHERE b.id = :blockId 
						AND b.Active__c=true 
						AND b.Name__c =:existingBlockList[0].Existing_Block_Name__c 
						AND b.Authentication_Required__c = false LIMIT 1];
						
		}
		if (existingBlockList1.size()>0 && existingBlockList1[0].Type__c == 
																	System.Label.Existing_Block) {
			existingBlockList1 = getExistingBlock(existingBlockList1);
		}
		
		
		return existingBlockList1;
	}
}