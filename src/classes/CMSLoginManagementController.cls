public without sharing class CMSLoginManagementController {
	public Contact contact { get; set; }
	
	public String userName { get; set; }
	
	public void init() {
		Id contactId = Login.getLoggedInContact();
		
		contact = (contactId != null)
				? [
						SELECT Name,
								CMSProfile__r.Default_Home_Page__r.Name__c,
								CMSProfile__r.Name__c
						FROM Contact
						WHERE id = :contactId]
				: new Contact();
		
		userName = contact.Id == null ? System.Label.Guest : contact.Name;
	}
	
	public PageReference logout() {
		delete [
				SELECT Session_For__c, CurrentDateTime__c, LastModifiedDate, SessionId__c
				FROM Session__c
				WHERE Is_Valid__c = true
				AND SessionId__c = :ApexPages.currentPage().getParameters().get('sessionId')];
    
		for (String sessionId : ApexPages.currentPage().getCookies().keySet()) {
			ApexPages.currentPage().setCookies(new List<Cookie> {
					new Cookie(sessionId, sessionId, null, 0, false) });
		}
		
		PageReference pg = new PageReference('/FCMS__CMSLayout');
		pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
		pg.getParameters().put('page', contact.CMSProfile__r.Default_Home_Page__r.Name__c);
		pg.getParameters().put('p', contact.CMSProfile__r.Name__c);
		pg.setRedirect(true);
		return pg;
	}
	
	public static testmethod void tstCMSLoginManagementController() {
		
		List<Account> accList = [SELECT id FROM Account WHERE Name = 'Candidate Pool2'];
		if (accList.size()>0) {
			delete accList;
		}
		
		Account account = new Account();
		account.Name = 'Candidate Pool2';
		insert account;
		List<CMSProfile__c> profileList = [SELECT id FROM CMSProfile__c];
		delete profileList;
		CMSProfile__c cmsProfile = new CMSProfile__c();
		cmsProfile.Name__c = 'CMSDefaultProfile';
		cmsProfile.Profile_Account__c = account.id;
		cmsProfile.CMSCss__c = 'CMSCssCustom';
		cmsProfile.User_Registration__c = true;
		insert cmsProfile;
		
		Contact contact = new Contact();
		contact.FirstName = 'Ramesh';
		contact.LastName = 'Dhull';
		contact.UserName__c = 'dhull.ramesh@gmail.com';
		contact.Email = 'dhull.ramesh@gmail.com';
		contact.Company_Name__c = 'uI1pZrEcalmTgcHQ0sZ6tstWh2pnQuY8URy4rlUUtrU=';
		contact.CMSProfile__c = cmsProfile.id;
		insert contact;
		Map<String,String> headerMap = System.currentPageReference().getHeaders();
		String sessionString;
		sessionString = String.valueOf(Math.random());
		sessionString = sessionString.substring(2,16);
		Session__c session = new Session__c();
		session.Session_For__c = contact.Id;
		session.User_Agent__c=headerMap.get('User-Agent');
		session.IP_Address__c=headerMap.get('X-Salesforce-SIP');
		session.SessionId__c = sessionString;
		insert session;
		PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
		pg.getParameters().put('sessionId',session.SessionId__c);
		Test.setCurrentPageReference(pg);
		Test.startTest();
		CMSLoginManagementController loginManagement = new CMSLoginManagementController();
		loginManagement.init();
		loginManagement.logout();
		
		Test.stopTest();
	}
}