@IsTest
private class SetPasswordControllerTest {
	@IsTest
	private static void test() {
		Contact contact = new Contact(LastName = 'Test', FCMS__UserName__c = 'un@gmail.com');
		insert contact;
		
		Test.startTest();
		
		SetPasswordController controller =
				new SetPasswordController(new ApexPages.StandardController(contact));
		System.assertEquals(null, controller.password);
		
		passwordIsValid(controller);
		
		Test.stopTest();
		
		contact = [
				SELECT FCMS__Company_Name__c
				FROM Contact
				WHERE Id = :contact.Id
		];
		System.assertEquals(
				'123456789', SetPasswordController.decryptString(contact.FCMS__Company_Name__c));
	}
	
	private static void passwordIsValid(SetPasswordController controller) {
		controller.password = '123456789';
		controller.save();
	}
}