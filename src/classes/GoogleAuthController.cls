/**
* @author Rakesh Rangu
* @date 09/29/2014
* @description An apex class used to login from Google
*/
public without sharing class GoogleAuthController extends SocialLoginHelper {
	public String access_token = '';
	public String body = '';
	public Map<String,String> userInfoMap = new Map<String,String>();

	public PageReference googleLogin() {
		System.debug('state parameter:'+ApexPages.currentPage().getParameters().get('state'));

		try {	
				String profileUrl = '';
				String code = ApexPages.currentPage().getParameters().get('code');
				String accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
				
				String jobSite = '';
				System.debug('Job Site Parameter:'+ApexPages.currentPage().getParameters().get('state'));
				if(ApexPages.currentPage().getParameters().get('state') != null) {
						
					ApexPages.currentPage().getParameters().put('jobSite',ApexPages.currentPage().getParameters().get('state'));
				}
				

				//If condition added as per case #00037602
				if(code != null) {
						
					body = 'code='+code+
							'&client_id='+ConfigHelper.getConfigDataSet().FCMS__Google_Client_Id__c+
							'&client_secret='+ConfigHelper.getConfigDataSet().FCMS__Google_Client_Secret__c+
							'&grant_type=authorization_code'+
							'&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__GoogleAuth';
							
	
					String tokenResp = postDataNew(accessTokenUrl,'POST');
					Response resp = (Response)JSON.deserialize(tokenResp,Response.class);
	
					UserDetails userInfo;
					String userResp = '';
	
					if (resp.access_token!=null) {
						access_token = resp.access_token;
						profileUrl = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+access_token;
						userResp = postDataNew(profileUrl, 'GET');
	
						if (userResp != 'error') {
	
							userInfo = (UserDetails)JSON.deserialize(userResp,UserDetails.class);
	
							userInfoMap.put('email',userInfo.email);
											userInfoMap.put('firstName',userInfo.given_name);
											userInfoMap.put('lastName',userInfo.family_name);
											userInfoMap.put('id',userInfo.id);
	
							System.debug('User Info:'+userInfoMap);
							PageReference pg = registerSocialUser(userInfoMap,'Google');
							return pg;
	
						}
	
					}
				} else {//else condition added as per case #00037602
					
					if (ApexPages.currentPage().getParameters().get('error') == 'access_denied') {
		            	System.debug('jobSite Parameter:'+ApexPages.currentPage().getParameters().get('state'));
		                FCMS__CMSProfile__c p = [SELECT FCMS__Name__c
		                FROM CMSProfile__c
		                WHERE User_Registration__c = true AND Order__c = 1
		                ORDER BY Order__c
		                LIMIT 1];
		                
		                PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
		                pg.getParameters().put('page', 'CMSSiteLogin');
		                pg.getParameters().put('p', p.FCMS__Name__c);
		                
		                if(ApexPages.currentPage().getParameters().get('state') != null) {
		                	pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('state'));
		                }
		                
		                pg.setRedirect(true);
		                return pg;
            		}
				}

		} catch(Exception e) {
			if (Test.isRunningTest()) {
				throw e;
			}
			//ApexPages.addMessages(e);
		}
		return null;
	}

		/**
		* @author Rakesh Rangu
		* @date 07/01/2014
		* @description Used to fetch access token from google
		*/
		public override String postDataNew(String url, String method) {
			System.debug('code:'+ApexPages.currentPage().getParameters().get('code'));
			HttpRequest req = new HttpRequest();
			HttpResponse res = new HttpResponse();
			Http http = new Http();

			req.setEndpoint(url);
			req.setMethod(method);
			req.setCompressed(false);
			req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

			if (method.equals('POST')) {
				req.setBody(body);
			} else {
				req.setHeader('Authorization', access_token);
					//req.serHeader('X-JavaScript-User-Agent','Google APIs Explorer');
			}


			res = http.send(req);
			if (res.getStatusCode() == 200) {
				System.debug('...info from server...'+res.getBody());
				return res.getBody();

			}else {
				System.debug('...failed to get info from server...');
				return 'error';
			}

		}

		public class Response {
			public String expires_in { get; set; }
			public String access_token { get; set; }
			public String token_type { get; set; }
			public String id_token { get; set; }
		}

		public class UserDetails {
			public String id { get; set; }
			public String email { get; set; }
			public String name { get; set; }
			public String given_name { get; set; }
			public String family_name { get; set; }
		}

}