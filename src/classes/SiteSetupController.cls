// TODO: Create inner abstract class and 3 implementations instead 
//		of methods of creating Account, Page and Profile. // Mikhail.
// TODO: Get rid of dull unit test and make brilliant in separate class. // Mikhail.

/**
* @author Mikhail Ivanov
* @date 12/29/2012
* @description Controller for page which has Setup button to create default demo CMS portal 
		and set default settings to Custom Settings.
*/
public class SiteSetupController {
	
	public void setup() {
		createDefaultCMSPortal();

		setupDefaultCustomSettings();
		
		ApexPages.addMessage(new ApexPages.Message(
				ApexPages.Severity.INFO, 
						System.Label.Configuration_setup_has_been_successfully_completedCMS));
	}
	
	private void createDefaultCMSPortal() {
		CMSProfile__c existingDefaultProfile = getExistingDefaultProfile();

		if (existingDefaultProfile == null) {
			Account accountForDefaultProfile = getFirstExistingAccountOrCreateNew();

			Page__c pageForDefaultProfile = getExistingDefaultPageOrCreateNew();

			createNewProfileWithDefaultAccountAndPage(
					accountForDefaultProfile, pageForDefaultProfile);
		}
	}
	
	private CMSProfile__c getExistingDefaultProfile() {
		List<CMSProfile__c> existingDefaultProfileInList = [
				SELECT Name, Profile_Manager__c, Profile_Account__c,
						Name__c, Default_Home_Page__c
				FROM CMSProfile__c
				WHERE Name__c = 'CMSDefaultProfile'
				LIMIT 1];
				
		return (existingDefaultProfileInList.size() == 1) 
				? existingDefaultProfileInList[0]
				: null;
	}
	
	private Account getFirstExistingAccountOrCreateNew() {
		List<Account> existingAccountInList = [SELECT Name FROM Account LIMIT 1];
		if (existingAccountInList.size() == 0) {
			Account newAccount = new Account(Name = 'CMSDefaultProfileAccount');
			insert newAccount;
			return newAccount;
		} else {
			return existingAccountInList[0];
		}
	}
	
	private Page__c getExistingDefaultPageOrCreateNew() {
		List<Page__c> existingPageInList = [
				SELECT Name,Name__c
				FROM Page__c
				WHERE Name__c = 'CMSDefaultProfilePage'
				LIMIT 1];
		if (existingPageInList.size() == 0) {
			Page__c newPage = new Page__c(
					Name = 'CMSDefaultProfilePage',
					Name__c = 'CMSDefaultProfilePage',
					Active__c = true);
			insert newPage;
			return newPage;
		} else {
			return existingPageInList[0];
		}
	}
	
	private void createNewProfileWithDefaultAccountAndPage(
			Account accountForDefaultProfile, Page__c pageForDefaultProfile) {
				
		CMSProfile__c cmsProfile = new CMSProfile__c();
		cmsProfile.Name = 'CMSDefaultProfile';
		cmsProfile.Name__c = 'CMSDefaultProfile';
		cmsProfile.Profile_Account__c = accountForDefaultProfile.Id;
		cmsProfile.Default_Home_Page__c = pageForDefaultProfile.Id;
		cmsProfile.Profile_Manager__c = UserInfo.getUserId();
		cmsProfile.Portal_Enabled__c = true;
		cmsProfile.User_Registration__c = true;
		cmsProfile = CMSProfileExtension.setupProfile(cmsProfile);
	}
	
	private void setupDefaultCustomSettings() {
		setupConfigSetting('FCMS__ErrorsReceiver__c', System.Label.ErrorsReceiver);
		setupConfigSetting('FCMS__ConfirmPasswordError__c', System.Label.ConfirmPasswordError);
		setupConfigSetting('FCMS__PasswordError__c', System.Label.PasswordError);
		setupConfigSetting('FCMS__ProfileUpdatedMsg__c', System.Label.ProfileUpdatedMsg);
		setupConfigSetting('FCMS__SessionTimeout__c', 600);
		setupConfigSetting('FCMS__Send_Logs__c', System.Label.Send_Logs);
		setupConfigSetting('FCMS__Captcha_failed_message__c', System.Label.Captcha_failed_message);
		setupConfigSetting('FCMS__Password_Email_Error__c', System.Label.Password_Email_Error);
		setupConfigSetting('FCMS__Candidate_Duplicate_Record_Error_Messa__c', 
				System.Label.Candidate_Duplicate_Record_Error_Messa);
				
		setupConfigSetting('FCMS__Candidate_Duplicate_Record_Email__c', 
				System.Label.Candidate_Duplicate_Record_Email);
	}

	private void setupConfigSetting(String configName, Object configDefaultValue) {
		Object obj = ConfigHelperCMS.getConfigSetting(configName);
		if (obj == null) {
			ConfigHelperCMS.setConfigSetting(configName, configDefaultValue);
		}
	}

	public static testmethod void tstSiteSetupController() {
		SiteSetupController siteSetup = new SiteSetupController();
		siteSetup.setup();
		
		SiteSetupController siteSetup1= new SiteSetupController();
		siteSetup1.setup();

		delete [SELECT Id FROM CMSProfile__c WHERE Name__c = 'CMSDefaultProfile'];
		delete [SELECT Id FROM Account WHERE Name = 'CMSDefaultProfileAccount1'];
		delete [SELECT Id FROM Page__c WHERE Name__c = 'CMSDefaultProfilePage'];

		SiteSetupController siteSetup2= new SiteSetupController();
		siteSetup2.setup();
	}
}