global interface FindDuplicates {
	Boolean hasDuplicates(Contact contact);
}