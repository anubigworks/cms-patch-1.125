public with sharing class SetEncryptedPassword implements Database.Batchable<SObject> {
	public Database.QueryLocator start(Database.BatchableContext bcMain) {
		return Database.getQueryLocator([
				SELECT FCMS__Password__c
				FROM Contact
				WHERE FCMS__Password__c != NULL AND FCMS__Company_Name__c = NULL
		]);
	}
	
	public void execute(Database.BatchableContext bc, List<SObject> objects) {
		List<Contact> contacts = (List<Contact>)objects;
		for (Contact contact : contacts) {
			contact.FCMS__Company_Name__c = encryptString(contact.FCMS__Password__c);
			contact.FCMS__Password__c = null;
		}
		
		update contacts;
	}
	
	private String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	private Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
	
	public void finish(Database.BatchableContext bc) {
	}
}