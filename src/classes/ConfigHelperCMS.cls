// Used to get values from Config object 
global without sharing class ConfigHelperCMS {
    global static CustomSettingsComponent__c getConfigDataSet() {
        return CustomSettingsComponent__c.getValues(System.Label.DefaultCMS);
    }
    
    // Gets config record by config name.
    public static Object getConfigSetting(String configName) {
        CustomSettingsComponent__c config = getConfigDataSet();
        return (config != null) ? config.get(configName) : null;        
    }
    
    public static void setConfigSetting(String configName, Object value) {
        CustomSettingsComponent__c config = getConfigDataSet();
        if (config == null) {
            config = setConfigDataSet(configName, value); 
        } else {   
            config.put(configName, value);
            update config;
        }
    }
    
    private static CustomSettingsComponent__c setConfigDataSet(String configName, Object value) {
        CustomSettingsComponent__c config = new CustomSettingsComponent__c(
        		Name = System.Label.DefaultCMS);
        		
        config.put(configName, value);
        insert config;
        
        return config;
    }
}