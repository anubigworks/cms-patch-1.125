public without sharing class DoLoginController {
	public PageReference login() {
		FCMS.CMSSiteLoginController loginController = new FCMS.CMSSiteLoginController();
		
		loginController.username = decryptString(
				ApexPages.currentPage().getParameters().get('username'));
		loginController.password = decryptString(
				ApexPages.currentPage().getParameters().get('password'));

		return loginController.login();
	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
}