/**
 * An apex page controller that exposes the site login functionality
 */

global without sharing class CMSSiteLoginController {
    global String selectLanguage { get; set; }
    global String username { get; set; }
    global String password { get; set; }
    
    //changed by rakesh on 07/11/2014
    global String linkedInUrl { get; set; }
    global String clientId { get; set; }
    global boolean render {get; set;}
    
    //added by Rakesh on 09/18/2014
    global String facebookUrl { get; set; }
    global String googleUrl { get; set; }
    global boolean fbRender { get; set; }
    global boolean gRender { get; set; }
    
    Map<String, CustomSettingsComponent__c> customSettingMap = CustomSettingsComponent__c.getAll();
    

    global String cmsCssCustom { get; set; }

    private String subject = System.Label.CMSSiteLoginController_Page_Error;

    global CMSSiteLoginController() {
        cmsCssCustom = CMSHelper.getCSSStyles();
        selectLanguage = UserInfo.getLocale();
        fbRender = false;
        gRender = false;
        
        //Added by Rakesh on 07/11/2014
        enableLinkedInLogin();
    }
    
    global void enableLinkedInLogin() {
        
        String p = ApexPages.currentPage().getParameters().get('p');
        String jobSite = '';
        
        system.debug('profile:'+p );
        if(p==null || p==''){
            
            render = false;
            
        }else {
            
            CustomSettingsComponent__c customSetting = customSettingMap.get('Default');
            system.debug('profile should not be empty');
            CMSProfile__c profile = [
                                SELECT Id,LinkedInLogin__c,FCMS__FacebookLogin__c,GoogleLogin__c,FCMS__Order__c
                                FROM CMSProfile__c
                                WHERE Name__c =: p
                                LIMIT 1];
            
            if(profile!=null){
                if(profile.LinkedInLogin__c && profile.FCMS__Order__c == 1){
                    system.debug('profile is true');
                    render=true;
                    if (customSettingMap != null) {
                        
                        if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
                            jobSite = ApexPages.currentPage().getParameters().get('jobSite');
                        }
                        else {
                            
                            jobSite = '';
                        }
                        clientId = customSetting.FCMS__LinkedIn_Client_Id__c;
                        String redirectUri = customSetting.FCMS__LinkedIn_Redirect_Uri__c;
                        //String redirectUri = 'https://fcms-developer-edition.ap1.force.com/';
                        if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
                            
                            linkedInUrl = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id='+clientId+'&scope=r_emailaddress,r_basicprofile&state=abcde12345&redirect_uri='+redirectUri+'FCMS__LinkedInAuth?jobSite='+jobSite;
                        } else {
                            
                            linkedInUrl = 'https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id='+clientId+'&scope=r_emailaddress,r_basicprofile&state=abcde12345&redirect_uri='+redirectUri+'FCMS__LinkedInAuth';
                        }
                    }
                }else {
                    render = false;
                }
                
                if(profile.FCMS__FacebookLogin__c && profile.FCMS__Order__c == 1){
                    
                    fbRender = true;
                    
                    
                    if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
                        
                        facebookUrl = 'https://www.facebook.com/dialog/oauth?'+
                          'client_id='+customSetting.FCMS__Facebook_Client_Id__c+
                             '&auth_type=reauthenticate&scope=public_profile, email'+
                             '&redirect_uri='+customSetting.FCMS__LinkedIn_Redirect_Uri__c+'FCMS__FacebookAuth?jobSite='+jobSite;
                    } else {
                        
                        facebookUrl = 'https://www.facebook.com/dialog/oauth?'+
                          'client_id='+customSetting.FCMS__Facebook_Client_Id__c+
                             '&redirect_uri='+customSetting.FCMS__LinkedIn_Redirect_Uri__c+'FCMS__FacebookAuth&scope=public_profile, email'+
                             '&auth_type=reauthenticate';
                    }
                    
                    
                }
                
                if(profile.GoogleLogin__c && profile.FCMS__Order__c == 1) {
                    gRender = true;
                    
                    
                    googleUrl = 'https://accounts.google.com/o/oauth2/auth?'+
                        'client_id='+customSetting.FCMS__Google_Client_Id__c+
                        '&response_type=code&scope=openid profile email'+
                        '&redirect_uri='+customSetting.FCMS__LinkedIn_Redirect_Uri__c+'FCMS__GoogleAuth'+
                        '&state='+jobSite;
                }
                
            } else{
                render = false;
                
            }
        }
    }
    
    
    global PageReference login() {
        try {
	        return CMSHelper.login(username, password);
        } catch (Exception e) {
            LoggerCMS.catchBlockMethod(e, subject);
        }

        return null;
    }
    
    private static testmethod void testFakeContactWithoutErrors() {
        ApexPages.currentPage().getParameters().put('p','Candidate');
        
        
        
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        Page__c testPage = new Page__c();

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
                                Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
                                User_Registration__c = true,
                                Force_com_Authorization_and_Registration__c = true);
        insert testProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        Contact testCon = new Contact(LastName = 'TestContact',
                                    UserName__c = 'testUser@test.test', Email = 'test@test.test',
                                    CMSProfile__c = testProfile.id,
                                    Registration_Approved__c = true,
		        Company_Name__c = 'testPassword');
        insert testCon;

        CMSSiteLoginController cmsSiteLogin = new CMSSiteLoginController();
        CMSSiteLogin.password = testCon.Company_Name__c;
        cmsSiteLogin.username = testCon.UserName__c;
        cmsSiteLogin.login();


    }

    private static testmethod void testFakeUserWithoutErrors() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        Page__c testPage = new Page__c();

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
                                Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
                                User_Registration__c = true,
                                Force_com_Authorization_and_Registration__c = false);
        insert testProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);

        Profile p = [SELECT id FROM profile  WHERE name = 'Customer Portal Manager Custom'  LIMIT 1];

        Contact contact = new Contact(
		        Company_Name__c = 'testPassword',
            accountId = testAcc.id,
            Firstname = 'test',
            lastname = 'test',
            email = 'test@mail.com');

        insert contact;

      /*  User pu = new User(
            profileId = p.id,
            username = 'testemail1@mail.com',
            email = 'testemail@mail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='cspu',
            lastname='lastname',
            contactId = contact.id);

        insert pu; */
        
        User user = new User();
        user.ProfileID = p.id;
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.FirstName = 'first';
        user.LastName = 'last';
        user.Username = 'test@appirio.com';
        user.CommunityNickname = 'testUser123';
        user.Alias = 't1';
        user.Email = 'no@email.com';
        user.IsActive = true;
        user.ContactId = contact.Id;
        
        insert user;

        System.RunAs(user) {
            CMSSiteLoginController cmsSiteLogin = new CMSSiteLoginController();
            CMSSiteLogin.password = contact.Company_Name__c;
            cmsSiteLogin.username = user.Username;
            cmsSiteLogin.login();
        }
        
    }

    private static testmethod void testFakeContactWithErrors() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        Page__c testPage = new Page__c();

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
                                Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
                                User_Registration__c = true,
                                Force_com_Authorization_and_Registration__c = false);
        insert testProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        Contact testCon = new Contact(LastName = 'TestContact',
                                    UserName__c = 'testUser@test.test', Email = 'test@test.test',
                                    CMSProfile__c = testProfile.id,
		        Company_Name__c = 'testPassword',
                                    Registration_Approved__c = false);
        insert testCon;

        CMSSiteLoginController cmsSiteLogin = new CMSSiteLoginController();
        CMSSiteLogin.password = testCon.Company_Name__c;
        cmsSiteLogin.username = testCon.UserName__c;
        cmsSiteLogin.login();
    }

    private static testmethod void testFakeContactWithErrors2() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        Page__c testPage = new Page__c();

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
                                Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
                                User_Registration__c = true,
                                Force_com_Authorization_and_Registration__c = false);
        insert testProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        Contact testCon = new Contact(LastName = 'TestContact',
                                    UserName__c = 'testUser@test.test', Email = 'test@test.test',
                                    CMSProfile__c = null,
		        Company_Name__c = 'testPassword',
                                    Registration_Approved__c = true);
        insert testCon;

        CMSSiteLoginController cmsSiteLogin = new CMSSiteLoginController();
        CMSSiteLogin.password = testCon.Company_Name__c;
        cmsSiteLogin.username = testCon.UserName__c;
        cmsSiteLogin.login();
    }

    private static testmethod void incorrectLogin(){
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;
        
        CMSProfile__c testProfile2 = new CMSProfile__c(Name__c = 'TestProfile',
                                Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
                                User_Registration__c = true,
                                Force_com_Authorization_and_Registration__c = false);
        insert testProfile2;
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile2.Name__c);
        Test.setCurrentPageReference(pg);
        CMSSiteLoginController cmsSiteLogin = new CMSSiteLoginController();
        CMSSiteLogin.password = '1231231231231231231';
        cmsSiteLogin.username = '3123123123123123123';
        cmsSiteLogin.login();
    }
}