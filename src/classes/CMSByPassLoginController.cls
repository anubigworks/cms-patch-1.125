/**
* @author Shashish kumar
* @date 12/12/2013
* @description Controller class is used for ByPassing login functionality of Register user.
*/

global without sharing class CMSByPassLoginController {
	
	private String username;
	private String password;
	
	public CMSByPassLoginController() {
	
	}
	
	/**
	* @author Shashish kumar
	* @date 12/12/2013
	* @description Method call the login method of CMSHelper class
	*				by passing the parameter as UserName and Password.
	*/
	
	global PageReference login() {
		String id = ApexPages.currentPage().getParameters().get('Id');
		
		if (!id.startsWith('003')) {
			// Param from URL converts '+' to ' '. So we must do the reversed step.
			id = id.replace(' ', '+');
			
			id = decryptString(id);
		}
		
		Contact contact = [
				SELECT UserName__c, Company_Name__c
				FROM Contact
				WHERE Id = :id
		];
		
		String userName = contact.UserName__c;
		String password = contact.Company_Name__c;
		
		PageReference pg = CMSHelper.login(userName, decryptString(password));
		
		if (pg != null) {
			String page = ApexPages.currentPage().getParameters().get('page');
			if (String.isNotBlank(page)) {
				pg.getParameters().put('page', page);
			}
			pg.setRedirect(true);
			return pg;
		}
		
		return null;
	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
}