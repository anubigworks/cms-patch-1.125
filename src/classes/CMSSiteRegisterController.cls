/**
 * An apex class that creates a portal user
 */
global without sharing class CMSSiteRegisterController {
    public String selectLanguage { get; set; }
    private Contact cont = new Contact();

    global String cmsCssCustom {get; set;}

    global CMSSiteRegisterController(){}

    global String getCmsCssCustom(){return null;}

    global String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }

    global Boolean profileFlag;
    global Boolean getProfileFlag() { return cmsProfileList.size() > 1; }

    global Contact getContact(){return cont;}
    global void setContact(Contact cont){this.cont = cont;}
    private Boolean displayError = false;
    private String subject = System.Label.CMSSiteRegisterController_Page_Error;
    public Boolean captchaIsShow {get;set;}

    //added by Rakesh on 07/10/2014
    public Boolean isSocialReg = false;

    global PageReference init() {
        
        selectLanguage = UserInfo.getLocale();
        cmsCssCustom = FCMS.CMSHelper.getCSSStyles();

        if((ConfigHelperCMS.getConfigDataSet().Candidate_Duplicate_Record_Email__c == null
                || ConfigHelperCMS.getConfigDataSet().Candidate_Duplicate_Record_Email__c == '') ||
                        (ConfigHelperCMS.getConfigDataSet().Candidate_Duplicate_Record_Error_Messa__c == null || ConfigHelperCMS.getConfigDataSet().Candidate_Duplicate_Record_Error_Messa__c == '')) {
                                LoggerCMS.addErrorMessage(System.Label.Site_is_temporarily_not_working);
        }

        captchaIsShow = ConfigHelperCMS.getConfigDataSet().Captcha_Enable__c;
        return null;
    }

    List<CMSProfile__c> cmsProfileList = [
            SELECT Profile_Manager__c,
                Profile_Manager__r.Email,
                Need_Registration_Approval__c,
                Approval_Message_Page__c,
                Approval_Message_Page__r.Name__c,
                User_Record_Type__c,
                Default_Home_Page__r.Name__c,
                Default_Home_Page__r.Id,
                Name__c,Profile_Account__c,
                Force_com_Authorization_and_Registration__c,
                Order__c
            FROM CMSProfile__c
            WHERE User_Registration__c = true
            ORDER BY Order__c];



    /**
    * @author Sergey Prishepo
    * @date 01/05/2013
    * @description Show on page in picklist value Candidate profile by default.case:00016110
    */
    global List<SelectOption> getItems()
    {
        List<SelectOption> v_optionsListForCandidateProfile = new List<SelectOption>();

        List<SelectOption> options = new List<SelectOption>();
        // TODO: Get the Profile record(cmsProfileList) from Database by using "Order By" Order__c field. Commented By : Shashish /12.06.2013
        for(Integer i = 0; i < cmsProfileList.size(); i++) {

            for(Integer j = i + 1; j < cmsProfileList.size(); j++) {

                if(cmsProfileList[i].Order__c > cmsProfileList[j].Order__c) {

                    CMSProfile__c profile = cmsProfileList[i];
                    cmsProfileList[i] = cmsProfileList[j];
                    cmsProfileList[j] = profile;
                }
            }
        }
        for(CMSProfile__c profile : cmsProfileList)
            options.add(new SelectOption(profile.id, profile.Name__c));

        v_optionsListForCandidateProfile.addAll(options);
        return v_optionsListForCandidateProfile;
    }

    global PageReference registerUser() {
        System.debug('registerUser');
        return registerUserForContact(cont, false);
    }

    public PageReference registerUserForContact(Contact cont, Boolean isSocialReg) {
        
        LoggerCMS.addMessage('CMSSiteRegisterController', 'registerUserForContact', '');
        
        try {
            if(!Test.isRunningTest() && !isSocialReg) {
                if (captchaIsShow && !captchaVerified()) {
                    LoggerCMS.addErrorMessage(System.Label.Captcha_is_incorrect);
                    return null;
                }
            }

            LoggerCMS.addMessage('CMSSiteRegisterController',
                    'registerUserForContact', 'cont.UserName__c = ' + cont.UserName__c);

            Boolean validUserName = Pattern.matches('^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$',cont.UserName__c);

            Boolean validEmail = Pattern.matches('^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$',cont.Email);

            if (!validEmail){
                LoggerCMS.addErrorMessage(System.Label.Invalid_Email);
                return null;
            }
            if (!validUserName){
                LoggerCMS.addErrorMessage(System.Label.Invalid_User_Name);
                return null;
            }
            
            CMSProfile__c profile;
            if (!isSocialReg) {
                profile = cmsProfileList.size() == 1
                        ? cmsProfileList[0]
                        : new Map<Id, CMSProfile__c>(cmsProfileList).get(cont.CMSProfile__c);
            } else {
                profile = [
                        SELECT Profile_Manager__c,
                            Profile_Manager__r.Email,
                            Need_Registration_Approval__c,
                            Approval_Message_Page__c,
                            Approval_Message_Page__r.Name__c,
                            User_Record_Type__c,
                            Default_Home_Page__r.Name__c,
                            Default_Home_Page__r.Id,
                            Name__c,Profile_Account__c,
                            Force_com_Authorization_and_Registration__c,
                            Order__c
                        FROM CMSProfile__c
                        WHERE User_Registration__c = true
                            AND Order__c = 1
                        ORDER BY Order__c];
            }
            
            if (cont.CMSProfile__c == null) {
                cont.CMSProfile__c = profile.Id;
            }
            
            FCMS__FindDuplicates__c cs = FCMS__FindDuplicates__c.getInstance();
            String namespace = cs.Namespace__c != null
                    ? cs.Namespace__c : 'FCMS';
            String className = cs.ClassName__c != null
                    ? cs.ClassName__c : 'FindDuplicatesImp';
            String namespaceInt = cs.NamespaceInternal__c != null
                    ? cs.NamespaceInternal__c : 'FCMS';
            String classNameInt = cs.ClassNameInternal__c != null
                    ? cs.ClassNameInternal__c : 'FindInternalDuplicates';
            
            LoggerCMS.addMessage('CMSSiteRegisterController',
                    'registerUserForContact', 'cs = ' + cs);
            
            FindDuplicates findDuplicates =
                    (FindDuplicates)Type.forName(namespace, className).newInstance();
            FindInternalDuplicates findInternalDuplicates =
                    (FindInternalDuplicates)Type.forName(namespaceInt, classNameInt).newInstance();

            Boolean hasDuplicates = findDuplicates.hasDuplicates(cont);
            
            LoggerCMS.addMessage('CMSSiteRegisterController',
                    'registerUserForContact', 'hasDuplicates = ' + hasDuplicates);
            
            if (hasDuplicates && isSocialReg) {
                CMSProfile__c selectedProfile = [
                        SELECT Profile_Manager__c,
                            Profile_Manager__r.Email,
                            Need_Registration_Approval__c,
                            Approval_Message_Page__c,
                            Approval_Message_Page__r.Name__c,
                            User_Record_Type__c,
                            Default_Home_Page__r.Name__c,
                            Default_Home_Page__r.Id,
                            Name__c,Profile_Account__c,
                            Force_com_Authorization_and_Registration__c,
                            Order__c
                        FROM CMSProfile__c
                        WHERE User_Registration__c = true AND Order__c = 1
                        ORDER BY Order__c];
                
                String jobSiteParam = '';
                if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
                    jobSiteParam = ApexPages.currentPage().getParameters().get('jobSite');
                }
                
                LoggerCMS.addErrorMessage(String.format(
                        System.Label.Social_Login_User_Exists,
                        new String[] { selectedProfile.Name__c, jobSiteParam }));
                return null;
            } else if (hasDuplicates && !isSocialReg) {
                LoggerCMS.addErrorMessage(System.Label.Candidate_Duplicate_Record_Error_Messa);
                return null;
            }
            
            if (cmsProfileList.size() == 0)
                LoggerCMS.addErrorMessage(System.Label.No_Profile_found_Please_contact_your_administrator);
            if (cont.FirstName == null || cont.FirstName == '')
                LoggerCMS.addErrorMessage(System.Label.First_name_is_required);
            if (cont.LastName == null || cont.LastName == '')
                LoggerCMS.addErrorMessage(System.Label.Last_name_is_required);
            if (cont.UserName__c == null || cont.UserName__c == '')
                LoggerCMS.addErrorMessage(System.Label.User_Name_is_required);
            if (cont.Email == null || cont.Email == '')
                LoggerCMS.addErrorMessage(System.Label.Email_is_required);

            List<Contact> contList = new List<Contact>();
            
            LoggerCMS.addMessage('CMSSiteRegisterController',
                    'registerUserForContact', 'isSocialReg = ' + isSocialReg);
            
            if (!isSocialReg) {
                if (cont.Company_Name__c == null || cont.Company_Name__c == '') {
                    LoggerCMS.addErrorMessage(System.Label.Password_is_required);
                }
                
                if (confirmPassword == '') {
                    LoggerCMS.addErrorMessage(System.Label.Password_confirmation_is_required);
                }
                
                if (cont.Company_Name__c != confirmPassword) {
                    LoggerCMS.addErrorMessage(Label.site.passwords_dont_match);
                }
                
                LoggerCMS.addMessage('CMSSiteRegisterController',
                        'registerUserForContact', 'cont = ' + cont);

                contList = findInternalDuplicates.getListOfInternalDuplicates(cont);
            } else {
                if (cont.LinkedInId__c != null) {
                    contList = [
                            SELECT UserName__c, Registration_Approved__c, Email, CMSProfile__c
                            FROM Contact
                            WHERE LinkedInId__c  = :cont.LinkedInId__c
                                AND UserName__c = :cont.UserName__c
                            LIMIT 1];
                } else if (cont.FCMS__GoogleId__c != null) {
                    contList = [
                            SELECT UserName__c, Registration_Approved__c, Email, CMSProfile__c
                            FROM Contact
                            WHERE FCMS__GoogleId__c  = :cont.FCMS__GoogleId__c
                            LIMIT 1];
                } else if (cont.FCMS__FacebookId__c != null) {
                    contList = [
                            SELECT UserName__c, Registration_Approved__c, Email, CMSProfile__c
                            FROM Contact
                            WHERE FCMS__FacebookId__c  = :cont.FCMS__FacebookId__c
                                AND UserName__c = :cont.UserName__c
                            LIMIT 1];
                }
            }
	
	        cont.Company_Name__c = encryptString(cont.Company_Name__c);
            
            if (contList != null && contList.size() > 0) {
                contList[0].FirstName = cont.FirstName;
                contList[0].LastName = cont.LastName;
                contList[0].Email = cont.Email;
                contList[0].UserName__c = cont.UserName__c.toLowerCase();
                contList[0].CMSProfile__c = cont.CMSProfile__c;
                
                if (String.isNotBlank(cont.Company_Name__c)) {
                    contList[0].Company_Name__c = cont.Company_Name__c;
                }
                
                cont = contList[0];
            }
            
            if (!ApexPages.hasMessages(ApexPages.Severity.ERROR)) {
                if (profile.Force_com_Authorization_and_Registration__c) {
                    User user = new User();
                    user.Username = cont.UserName__c;
                    user.Email = cont.Email;
                    user.FirstName = cont.FirstName;
                    user.LastName = cont.LastName;
                    user.CommunityNickname = cont.FirstName + ' '+ cont.LastName;

                    String accountId = profile.Profile_Account__c;
                    String firstNameContact = cont.FirstName;
                    String lastNameContact = cont.LastName;
                    String password = cont.Company_Name__c;
                    String userName = cont.UserName__c;

                    Savepoint sp = Database.setSavepoint();
                    
                    String userId = Site.createPortalUser(
		                    user, accountId, decryptString(cont.Company_Name__c));
                    if (userId == null) {
                        Database.rollback(sp);
                        return null;
                    }
                    
                    List<User> customerPortalUser = [
                        SELECT Contact.Id, Contact.FirstName,Contact.LastName, Contact.Company_Name__c,
                                Contact.UserName__c
                        FROM User
                        WHERE UserName = :cont.UserName__c];

                    try {
                        if (customerPortalUser.size() > 0) {
                            cont = customerPortalUser[0].Contact;
                            cont.FirstName = firstNameContact;
                            cont.LastName = lastNameContact;
                            cont.Company_Name__c = password;
                            cont.UserName__c = userName;
                        }
                    } catch(Exception e) {
                        return null;
                    }
                }
                
                cont.CMSProfile__c = profile.Id;
                cont.AccountId = profile.Profile_Account__c;
                cont.Profile_Manager_Email__c = profile.Profile_Manager__r.Email;
                cont.Registration_Approved__c = !profile.Need_Registration_Approval__c;
                cont.PortalEmailAlert__c = System.Label.User_Created;
                cont.LeadSource = System.Label.OnlineCMS;

                Map<String,Schema.RecordTypeInfo> recordTypes =
                        Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
                Schema.RecordTypeInfo rtInfo = recordTypes.get(profile.User_Record_Type__c);
                if (rtInfo != null) {
                    cont.RecordTypeId = rtInfo.getRecordTypeId();
                }
                
                upsert cont;
                
                return reDirect(cont,profile);
            }
        }
        catch (Exception e)
        {
            if(e instanceOf System.DmlException && e.getDmlStatusCode(0) == 'FIELD_CUSTOM_VALIDATION_EXCEPTION') {

                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0) );
                ApexPages.addMessage(msg);
            }
            else{
                LoggerCMS.catchBlockMethod(e, subject);

            }
        }

        return null;
    }
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}

    //added by Rakesh on 29/07/2014
    global PageReference reDirect(Contact cont, CMSProfile__c profile) {
        if(cont.Registration_Approved__c) {
            return CMSHelper.doLogin(cont,
                profile.Name__c,
                profile.Default_Home_Page__r.Name__c);
        }
        else {
            PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
            pg.getParameters().put('page', profile.Approval_Message_Page__r.Name__c);
            pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
            pg.getParameters().put('p', profile.Name__c);
            pg.setRedirect(true);
            System.debug('page reference:'+pg);
            return pg;
        }

    }

    //From CMSController
    global PageReference getLoginPage() {
        Pagereference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getparameters().put('page','CMSSiteLogin');
        pg.getParameters().put('jobSite',ApexPages.currentPage().getParameters().get('jobSite'));
        pg.getParameters().put('p',ApexPages.currentPage().getParameters().get('p'));
        pg.getParameters().put('startUrl',ApexPages.currentPage().getParameters().get('startUrl'));
        return pg;
    }

    /*************reCaptcha********************************/
    private static string secret = '6Lcuj8QSAAAAAOBuTKNOIbFUjXXav6X8tZVxb-ns';
    public string publicKey { get { return '6Lcuj8QSAAAAAAEYREXBgp9PKhYIDpA-hnsATOvV'; }}

    private static string baseUrl = 'http://www.google.com/recaptcha/api/verify';  //'http://api-verify.recaptcha.net/verify';

    public string challenge {get; set;}
    public string response {get; set; }

    public Boolean captchaVerified() {
        LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', '');
        System.debug('==== challenge > ' + challenge + ' ==== response > ' + response);
        if (challenge != null && response != null) {
            System.debug(logginglevel.ERROR, '::challenge != null && response != null' + challenge + ' '+ response);
            LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'challenge=' + challenge);
            LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'response=' + response);
            LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'baseUrl=' + baseUrl);
            System.debug('!!!!!!!!!!!!baseUrl=' + baseUrl);

            HttpResponse r = makeRequest(baseUrl,
                'privatekey=' + secret +
                '&remoteip=' + remoteHost +
                '&challenge=' + challenge +
                '&response=' + response +
                '&error=incorrect-captcha-sol');

            LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'baseUrl=' + baseUrl);
            System.debug('!!!!!!!!!!1!baseUrl=' + baseUrl);

            LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'r=' + r);

            System.debug(logginglevel.ERROR, '::r != null' + r);
            
            // is null when test methods run
            if (r != null)
            {
                System.debug(logginglevel.ERROR, '::r != null' + r.getBody().contains('true'));

                LoggerCMS.addMessage('JobApplicationComponentController1', 'captchaVerified', 'r.getBody()=' + r.getBody());
                return r.getBody().contains('true');
            }
        }

        System.debug(logginglevel.ERROR, '::return');
        return false;
    }

    public static HttpResponse makeRequest(string url, string body) {
        LoggerCMS.addMessage('JobApplicationComponentController1', 'makeRequest', 'url='+ url);

        System.debug('!!!!!!!!!!!!url=' + url);

        LoggerCMS.addMessage('JobApplicationComponentController1', 'makeRequest', '!!!!!!!!!!!!body='+ body);

        System.debug('!!!!!!!!!!1!body=' + body);
        
        HttpRequest req = new HttpRequest();
        HttpResponse response = null;
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody(body);

        LoggerCMS.addMessage('JobApplicationComponentController1', 'makeRequest', '!!!!!!!!!!!!req='+ req);

        System.debug('!!!!!!!!!!!!req=' + req);
        try {
            Http http = new Http();
            response = http.send(req);

            System.debug('!!!!!req=' + req);
            System.debug('::response: '+ response);
            System.debug('::body: '+ response.getBody());
        }
        catch(System.Exception e) {
                System.debug('::ERROR: '+ e);
        }
        return response;
    }

    public string remoteHost {
        get {
            string ret = '127.0.0.1';
            // also could use x-original-remote-host
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr') != null)
                ret = hdrs.get('x-original-remote-addr');
            else if(hdrs.get('X-Salesforce-SIP') != null)
                ret = hdrs.get('X-Salesforce-SIP');
                System.Debug('!!!!!!!!!!!ret='+ ret);
                return ret;
            }
    }

    /*************reCaptcha end********************************/

    global static testmethod void tstCMSSiteRegisterController() { }

    static testmethod void registerContactTestScenario() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        Page__c testPage = new Page__c(Name = 'TestPage', Name__c = 'TestPage');

        CMSProfile__c testCMSProfile = new CMSProfile__c( Name = 'TestProfile',
            Need_Registration_Approval__c = true,
            Name__c = 'TestProfile',
            Profile_Account__c = testAcc.id,
            Force_com_Authorization_and_Registration__c = false,
            User_Registration__c = true,
            Default_Home_Page__c = testPage.id);
            
        insert testCMSProfile;

        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testCMSProfile.Name__c);
        Test.setCurrentPageReference(pg);
        CMSSiteRegisterController controller = new CMSSiteRegisterController();

        controller.init();

        Map<String,Schema.RecordTypeInfo> recordTypes = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();

        controller.setContact(new Contact(Company_Name__c = 'testPassword12',
            FirstName = 'test', LastName = 'test', Email = 'asddsa@asddsa.com',
            UserName__c = 'asddsa@asddsa.com', CMSProfile__c = testCMSProfile.id,
            RecordTypeId = recordTypes.values()[1].getRecordTypeId(),
            Registration_Approved__c = true));

        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP','X-Salesforce-SIP');

        controller.confirmPassword = controller.getContact().Company_Name__c;

        controller.getCmsCssCustom();
        controller.getProfileFlag();
        controller.getContact();
        controller.getItems();

        controller.challenge = 'qqqq';
        controller.response = 'qqqq';
        controller.captchaVerified();
        //controller.isSocialReg  = true;
        String remoteHost = controller.remoteHost;

        controller.registerUser();
        controller.isSocialReg = true;
        controller.registerUser();

        controller.getLoginPage();
    }

    static testmethod void incorrectRegisterTest() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        CMSProfile__c testCMSProfile = new CMSProfile__c( Name = 'TestProfile',
            Need_Registration_Approval__c = true,
            Name__c = 'TestProfile',
            Force_com_Authorization_and_Registration__c = false,
            User_Registration__c = false);
            
        insert testCMSProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testCMSProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        ApexPages.currentPage().getHeaders().put('x-original-remote-addr','x-original-remote-addr');

        CMSSiteRegisterController controller = new CMSSiteRegisterController();

        controller.init();

        controller.setContact(new Contact(Company_Name__c = 'testPassword12',
            FirstName = 'test', LastName = 'test', Email = 'asddsa@asddsa.com',
            UserName__c = 'asddsa@asddsa.com', CMSProfile__c = testCMSProfile.id));

        controller.confirmPassword = controller.getContact().Company_Name__c;

        //controller.cmsProfileList
        controller.getCmsCssCustom();
        controller.getProfileFlag();
        controller.getContact();
        controller.getItems();
        controller.challenge = 'qqqq';
        controller.response = 'qqqq';
        controller.captchaVerified();
        //controller.isSocialReg  = true;
        String remoteHost = controller.remoteHost;
        controller.registerUser();
    }

    static testmethod void registerFromOtherSource() {
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
            Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
            User_Registration__c = true,
            Force_com_Authorization_and_Registration__c = false);
            
        insert testProfile;

        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        Contact testCon = new Contact(LastName = 'TestContact',
            UserName__c = 'testUser@test.test', Email = 'test@test.test',
            CMSProfile__c = testProfile.id,
            Registration_Approved__c = true,
		        Company_Name__c = 'testPassword');
          
        insert testCon;

        CMSSiteRegisterController controller = new CMSSiteRegisterController();

        controller.confirmPassword = controller.getContact().Company_Name__c;

        //controller.cmsProfileList
        controller.getCmsCssCustom();
        controller.getProfileFlag();
        controller.getContact();
        controller.getItems();
        controller.challenge = 'qqqq';
        controller.response = 'qqqq';
        controller.captchaVerified();
        //controller.isSocialReg  = true;
        String remoteHost = controller.remoteHost;
        controller.registerUserForContact(testCon, true);

    }

    static testmethod void registerErrors(){
        Account testAcc = new Account(Name = 'TestAcc');
        insert testAcc;

        String usrName = Integer.valueOf(math.rint(math.random()*1000000)) + '@test' +  UserInfo.getOrganizationId() + '.com';

        CMSProfile__c testProfile = new CMSProfile__c(Name__c = 'TestProfile',
            Profile_Account__c = testAcc.id, CMSCss__c = 'testCMS',
            User_Registration__c = true,
            Force_com_Authorization_and_Registration__c = true);
            
        insert testProfile;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('p',testProfile.Name__c);
        Test.setCurrentPageReference(pg);
        
        User testUser = new User(
            UserName = usrName,
            LastName = 'User #1',
            Alias = 'User #1',
            CommunityNickname = 'User #1',
            TimeZoneSidKey = 'America/Chicago',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = UserInfo.getProfileId(),
            LanguageLocaleKey = 'en_US',
            Email = 'em1@test.com'
        );

        insert testUser;

        Contact testCon = new Contact(LastName = 'TestContact',
            Registration_Approved__c = true,
            UserName__c = usrName,
            FirstName = 'test',
		        Company_Name__c = 'test',
            AccountId = testacc.Id,
            CMSProfile__c = testProfile.id
        );
        
        CMSSiteRegisterController controller = new CMSSiteRegisterController();
        controller.cmsProfileList = [
            SELECT Profile_Manager__c,
            Profile_Manager__r.Email,
            Need_Registration_Approval__c,
            Approval_Message_Page__c,
            Approval_Message_Page__r.Name__c,
            User_Record_Type__c,
            Default_Home_Page__r.Name__c,
            Default_Home_Page__r.Id,
            Name__c,Profile_Account__c,
            Force_com_Authorization_and_Registration__c,
            Order__c
            FROM CMSProfile__c
            WHERE User_Registration__c = true
            ORDER BY Order__c];
            
        controller.registerUserForContact(testCon, true);
        controller.registerUserForContact(testCon, true);
        Contact testCon1 = new Contact(LastName = 'TestContact',
            Registration_Approved__c = true,
            UserName__c = 'test@yahoo.com',
            FirstName = 'test',
		        Company_Name__c = 'test',
            AccountId = testacc.Id,
            CMSProfile__c = testProfile.id
        );
        
        controller.registerUserForContact(testCon1, true);
        controller.confirmPassword = controller.getContact().Company_Name__c;

        testCon.Email = null;
        controller.registerUserForContact(testCon, true);

        testCon.Email = 'test';
        controller.registerUserForContact(testCon, true);

        testCon.Email = 'test@yahoo.com';
        controller.registerUserForContact(testCon, true);

        controller.cmsProfileList = controller.cmsProfileList;
        controller.registerUserForContact(testCon, true);

        testcon.Company_Name__c = null;
        controller.confirmPassword = '';
        controller.registerUserForContact(testCon, false);

        testcon.Company_Name__c = 'test';
        controller.confirmPassword = '';
        controller.registerUserForContact(testCon, false);

        testcon.Company_Name__c = 'test';
        controller.confirmPassword = 'test';
        controller.registerUserForContact(testCon, false);

        testcon.FirstName = null;

        controller.registerUserForContact(testCon, false);

        testcon.FirstName = 'test';
        controller.registerUserForContact(testCon, false);

        testcon.lastName = null;
        controller.registerUserForContact(testCon, false);

        testcon.lastName = 'test1';
        controller.registerUserForContact(testCon, true);

        testcon.UserName__c = null;
        controller.registerUserForContact(testCon, true);

        testcon.UserName__c = 'test1';
        controller.registerUserForContact(testCon, true);

        testcon.UserName__c = 'test1@yahoo.com';
        controller.registerUserForContact(testCon, true);

        controller.cmsProfileList = new List<CMSProfile__c>();
        controller.registerUserForContact(testCon, false);
        
    }
}