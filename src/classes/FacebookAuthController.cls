/**
* @author Rakesh Rangu
* @date 09/29/2014
* @description An apex class used to login from Facebook
*/
public without sharing class FacebookAuthController extends SocialLoginHelper{

	public Map<String,String> userInfoMap = new Map<String,String>();

	/**
	* @author Rakesh Rangu
	* @date 09/29/2014
	* @description Facebook Login functionality
	*/
	public PageReference facebookLogin() {
		
		
		try {
				String code = ApexPages.currentPage().getParameters().get('code');
				System.debug('code:'+code);
				String accessTokenUrl = '';
				
				//If condition added as per case #00037602
				if(code != null) {
					if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
						if(ApexPages.currentPage().getParameters().get('jobSite') == '') {
							accessTokenUrl = 'https://graph.facebook.com/oauth/access_token?'+
											'client_id='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Id__c+										
											'&client_secret='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Secret__c+
											'&code='+code+
											'&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__FacebookAuth?jobSite';
						} else {
							
							accessTokenUrl = 'https://graph.facebook.com/oauth/access_token?'+
											'client_id='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Id__c+										
											'&client_secret='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Secret__c+
											'&code='+code+
											'&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__FacebookAuth?jobSite='+ApexPages.currentPage().getParameters().get('jobSite');
						}
											
					} else {
						
						accessTokenUrl = 'https://graph.facebook.com/oauth/access_token?'+
											'client_id='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Id__c+										
											'&client_secret='+ConfigHelper.getConfigDataSet().FCMS__Facebook_Client_Secret__c+
											'&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__FacebookAuth'+
											'&code='+code	;
						
					}
	
					String tokenResp = postDataNew(accessTokenUrl,'GET');
					String accessToken = '';
	
					if (tokenResp != 'error') {
						List<String> respString = tokenResp.split('&expires=');
						accessToken = respString[0].replace('access_token=', '');
	
						String profileUrl = 'https://graph.facebook.com/me?access_token='+accessToken+'&fields=id,name,email,first_name,last_name';
						String userResp = postDataNew(profileUrl,'GET');
						UserDetails userInfo;
	
						if (userResp != 'error') {
							userInfo = (UserDetails)JSON.deserialize(userResp,UserDetails.class);
	
							userInfoMap.put('email',userInfo.email);
							userInfoMap.put('firstName',userInfo.first_name);
							userInfoMap.put('lastName',userInfo.last_name);
							userInfoMap.put('id',userInfo.id);
	
							System.debug('User Info:'+userInfoMap);
							PageReference pg = registerSocialUser(userInfoMap,'Facebook');
							return pg;
	
						}
	
					}
				} else { //else condition added as per case #00037602
					if (ApexPages.currentPage().getParameters().get('error') == 'access_denied') {
		            	System.debug('jobSite Parameter:'+ApexPages.currentPage().getParameters().get('jobSite'));
		                FCMS__CMSProfile__c p = [SELECT FCMS__Name__c
		                FROM CMSProfile__c
		                WHERE User_Registration__c = true AND Order__c = 1
		                ORDER BY Order__c
		                LIMIT 1];
		                
		                PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
		                pg.getParameters().put('page', 'CMSSiteLogin');
		                pg.getParameters().put('p', p.FCMS__Name__c);
		                
		                if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
		                	pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
		                }
		                
		                pg.setRedirect(true);
		                return pg;
					}
				}

			} catch(Exception e) {
				if (Test.isRunningTest()) {
					throw e;
				}

			//ApexPages.addMessages(e);
		}

		return null;
	}



	/**
	* @author Rakesh Rangu
	* @date 09/29/2014
	* @description Response class for fetching access token
	*/
	public class UserDetails {

		public String first_name { get; set; }
		public String last_name { get; set; }
		public String email { get; set; }
		public String id { get; set; }
	}

}