/**
* @description Helper class for Grouping fields based on various criteria like by Id, etc.
*/
public without sharing class GroupByHelper {
	/**
* @name groupByField
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Object, List<sObject>>
*/
	// Usage: Map<Object, List<Time__c>> varName = (Map<Object, List<Time__c>>)GroupByHelper.groupByField(times, 'Case__c');
	public static Map<Object, List<sObject>> groupByField(
			List<sObject> recordsToGroup, String keyField) {
		
		Map<Object, List<sObject>> keyToRecordsMap = new Map<Object, List<sObject>>();

		if (recordsToGroup != null) {
			for (sObject record : recordsToGroup) {
				Object key = MetadataUtil.getValue(record, keyField);

				List<sObject> recordList = keyToRecordsMap.get(key);
				if (recordList == null) {
					recordList = new List<sObject>();
					keyToRecordsMap.put(key, recordList);
				}

				recordList.add(record);
			}
		}

		return keyToRecordsMap;
	}

	/**
* @name groupByFieldKeyId
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Id, List<sObject>>
*/
	public static Map<Id, List<sObject>> groupByFieldKeyId(
			List<sObject> recordsToGroup, String keyField) {

		Map<Id, List<sObject>> mapKeyId = new Map<Id, List<sObject>>();

		Map<Object, List<sObject>> mapKeyObject = groupByField(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			Id keyId = (Id)keyObj;
			mapKeyId.put(keyId, mapKeyObject.get(keyObj));
		}

		return mapKeyId;
	}

	/**
* @name groupByFieldReturnSingle
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Object, sObject>
*/
	public static Map<Object, sObject> groupByFieldReturnSingle(
			List<sObject> recordsToGroup, String keyField) {

		Map<Object, sObject> result = new Map<Object, sObject>();

		Map<Object, List<sObject>> mapKeyObject = groupByField(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			result.put(keyObj, mapKeyObject.get(keyObj)[0]);
		}
		
		return result;
	}

/**
* @name groupByIdToObject
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Id, sObject>
*/
	public static Map<Id, sObject> groupByIdToObject(
			List<sObject> recordsToGroup, String keyField) {

		Map<Id, sObject> result = new Map<Id, sObject>();

		Map<Object, sObject> mapKeyObject = groupByFieldReturnSingle(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			result.put((Id)keyObj, mapKeyObject.get(keyObj));
		}

		return result;
	}
	
	public static List<Date> getFieldValuesDates(List<sObject> records, String keyField) {
		List<Date> fieldValues = new List<Date>();
		
		if (records != null) {
			for (sObject record : records) {
				fieldValues.add((Date)MetadataUtil.getValue(record, keyField));
			}
		}

		return fieldValues;
	}
	

/**
* @name getFieldValuesIds
* @description Gets Set of field value ID.
* @param records List<sObject>
* @param keyField String
* @return Set<Id>
*/
	// Usage: Set<Id> varName = GroupByHelper.getFieldValuesIds(records, 'ID_Field');
	public static Set<Id> getFieldValuesIds(List<sObject> records, String keyField) {
		Set<Id> fieldValues = new Set<Id>();

		if (records != null) {
			for (sObject record : records) {
				fieldValues.add((Id)MetadataUtil.getValue(record, keyField));
			}
		}

		return fieldValues;
	}
	
	public static Set<String> getFieldValuesStringsSet(List<sObject> records, String keyField) {
		Set<String> fieldValues = new Set<String>();
		
		if (records != null) {
			for (sObject record : records) {
				fieldValues.add((String)MetadataUtil.getValue(record, keyField));
			}
		}
		
		return fieldValues;
	}
	
	/**
* @name getFieldValuesStrings
* @description Gets Set of field value String.
* @param records List<sObject>
* @param keyField String
* @return Set<String>
*/
	public static List<String> getFieldValuesStrings(List<sObject> records, String keyField) {
		List<String> fieldValues = new List<String>();

		for (sObject record : records) {
			fieldValues.add((String)MetadataUtil.getValue(record, keyField));
		}

		return fieldValues;
	}
}