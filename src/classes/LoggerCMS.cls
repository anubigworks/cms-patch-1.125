global with sharing class LoggerCMS {
	private static string messages = '';

	global static void addMessage(String className, String methodName, String message) {
		
		String logMessage =
				'Class name: \'' + className +
			 '\' Method name: \'' + methodName +
			 '\' Message: \'' + message + '\'';

		System.debug(LoggingLevel.ERROR, logMessage);

		messages += logMessage + '\r\n\r\n';
	}

	global static void catchBlockMethod(Exception e, String subject) {
		if (e instanceOf System.DmlException) {
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getDmlFieldNames(0)=' + e.getDmlFieldNames(0));
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getDmlFields(0)=' + e.getDmlFields(0));
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getDmlStatusCode(0)=' + e.getDmlStatusCode(0));
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getDmlMessage(0)=' + e.getDmlMessage(0));
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getDmlType(0)=' + e.getDmlType(0));
			addMessage('LoggerCMS', 'catchBlockMethod', 'e.getNumDml()=' + e.getNumDml());

			if (e.getDmlStatusCode(0) == 'FIELD_CUSTOM_VALIDATION_EXCEPTION') {
				addErrorMessage(e.getDmlMessage(0));
				return;
			}
		}

		addMessage('LoggerCMS', 'catchBlockMethod', 'e.getMessage()=' + e.getMessage());
		addMessage('LoggerCMS', 'catchBlockMethod','getCause()=' + e.getCause());
		addMessage('LoggerCMS', 'catchBlockMethod','getLineNumber()=' + e.getLineNumber());
		addMessage('LoggerCMS', 'catchBlockMethod','getStackTraceString()=' + e.getStackTraceString());
		addMessage('LoggerCMS', 'catchBlockMethod','getTypeName()=' + e.getTypeName());


		addErrorMessage(System.Label.An_error_has_occured_in_the_application_process);
		
		/* Added by Aliaksandr Satskou, 07/17/2013 (case #00019977, point 2) */
		try {
			sendError(e.getMessage(), subject);
		} catch (Exception ex) {
			System.debug(LoggingLevel.ERROR, ex);
		}
	}

	global static void addErrorMessage(String message) {
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
		ApexPages.addMessage(msg);
	}

	global static void sendLog(String subject) {
		Boolean sendLog = (Boolean)ConfigHelperCMS.GetConfigSetting('FCMS__Send_Logs__c');
		
		if (sendLog != null && sendlog) {
			/* Added by Aliaksandr Satskou, 07/17/2013 (case #00019977, point 2) */
			try {
				sendError('Test Log', subject);
			} catch (Exception e) {
				System.debug(LoggingLevel.ERROR, e);
			}
		}
	}

	global static void sendError(String lastMessage, String subject) {
		if (Site.getName() != null) {
			messages =
					'Organization: \'' + UserInfo.getOrganizationName() + '\'\r\n' +
					'Site Name: \'' + Site.getName() + '\'\r\n' +
					'Site Url: \'' + Site.getCurrentSiteUrl() + '\'\r\n' +
					'Page Url: \'' + ApexPages.currentPage().getUrl() + '\'\r\n\r\n' +
					messages;
		} else {
			messages =
					'Organization: \'' + UserInfo.getOrganizationName() + '\'\r\n\r\n' +
					messages;
		}

		String fullLog = messages + lastMessage;

		Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
		emailMessage.setSubject(subject);
		emailMessage.setPlainTextBody(fullLog);
		emailMessage.setToAddresses(new List<String> {
				ConfigHelperCMS.getConfigDataSet().ErrorsReceiver__c });
		
		/* Added by Aliaksandr Satskou, 07/17/2013 (case #00019977, point 2) */
		try {
			if(!test.isRunningTest()){
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMessage});

			}
		} catch (Exception e) {
			System.debug(LoggingLevel.ERROR, e);
		}

		if (fullLog.length() > 31950) {
			fullLog = fullLog.substring(fullLog.length() - 31950);
		}

		insert new CMSSiteSetup__c(
				Name__c = '' + DateTime.now(),
				Value__c = fullLog);
	}

	private static testmethod void testLoggerCMS() {
		try {
			addMessage('Test', 'Test', 'Test');

			// Generates exception
			Integer i = 100/0;
		} catch(Exception e) {
			catchBlockMethod(e, 'Test');
		}
		LoggerCMS.sendLog('Test');
	}
}