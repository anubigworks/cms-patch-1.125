global without sharing class ContactFieldsUtil {
    global static Map<string, Schema.SObjectField> fieldMap {
        get {
            if (fieldMap == null) {
                fieldMap = Schema.SObjectType.Contact.fields.getMap();
            }
                
            return fieldMap;
        }
        set;
    }
    
    
    global static String getAllFieldsString() {
        String result = '';
        
        for (Schema.SObjectField field : fieldMap.values()) {
            result += ',' + field.getDescribe().Name;
        }
            
        return result.substring(1);
    }
    
    
    global static void updateRecords(sObject source, sObject target) {
        LoggerCMS.addMessage('ContactFieldsUtil', 'updateRecords', 'start');
        
        for (string key : fieldMap.keySet()) {
            Schema.SObjectField f = fieldMap.get(key);  
            Boolean access = f.getDescribe().isAccessible();
			
			try {
				if ((access) && (key != 'ownerId')) {
	                Object valueSource = source.get(key);
	                Object valueTarget = target.get(key);
	                
	                if ((valueSource != valueTarget) && (valueSource != null)) {
	                	if (((valueTarget == false) || (valueTarget == true)) 
	                			&& valueSource == null) {
	                				
	                		target.put(key, valueTarget);
	                	} else {
	                		target.put(key, valueSource);
	                	}
	                }
	            }
            } catch(Exception ex) { }  
        }
        LoggerCMS.addMessage('ContactFieldsUtil', 'updateRecords', 'finish');
	}
}