global without sharing class Login {
	
	private static Boolean isContactIdIdentified = false;
	private static Id contactIdIdentified;

	global static Id getLoggedInContact() {
		if (isContactIdIdentified) {
			return contactIdIdentified;
		}
		isContactIdIdentified = true;

		LoggerCMS.addMessage('Login', 'getLoggedInContact', '');

		String sessionId = ApexPages.currentPage().getParameters().get('sessionId');

		LoggerCMS.addMessage('Login', 'getLoggedInContact', 'sessionId=' + sessionId);

		if (sessionId != null && sessionId.length() > 0) {
			LoggerCMS.addMessage('Login', 'getLoggedInContact', 'sessionId found');

			List<Session__c> sessionList = [
					SELECT Session_For__c, CurrentDateTime__c, LastModifiedDate, SessionId__c
					FROM Session__c
					WHERE Is_Valid__c = true
					AND SessionId__c = :sessionId];

			LoggerCMS.addMessage('Login', 'getLoggedInContact', 'sessionList=' + sessionList);

			if (sessionList.size() > 0) {
				Session__c session = sessionList[0];

				Decimal timeOut = ConfigHelperCMS.getConfigDataSet().SessionTimeout__c;
				if (session.CurrentDateTime__c < session.LastModifiedDate.addHours(timeOut.intValue())) {
					Cookie sessionCookie = 
							ApexPages.currentPage().getCookies().get(session.SessionId__c);

					if (sessionCookie != null) {
						LoggerCMS.addMessage('Login', 'getLoggedInContact', 
								'session.Session_For__c=' + session.Session_For__c);
						
						contactIdIdentified = session.Session_For__c;
					}
				}
			}
		}

		LoggerCMS.addMessage('Login', 'getLoggedInContact', 
				'contactIdIdentified=' + contactIdIdentified);

		return contactIdIdentified;
	}

	public static String getUserAgent() {

		Map<String,String> headerMap = System.currentPageReference().getHeaders();

		LoggerCMS.addMessage('Login', 'getUserAgent', 'headerMap=' + headerMap);

		String userAgent = headerMap.get('User-Agent');
		if(userAgent != null && userAgent.trim().length() > 255)
			userAgent = userAgent.trim().substring(0, 254);

		LoggerCMS.addMessage('Login', 'getUserAgent', 'userAgent=' + userAgent);

		return userAgent;
	}

	public static String getIP() {

		Map<String,String> headerMap = System.currentPageReference().getHeaders();

		LoggerCMS.addMessage('Login', 'getIP', 'headerMap=' + headerMap);

		String ip = headerMap.get('X-Salesforce-SIP');

		LoggerCMS.addMessage('Login', 'getIP', 'ip=' + ip);

		return ip;
	}
}