global without sharing class CMSHelper {
	private static Boolean isContactIdentified = false;
	private static Contact contactIdentified;
	
	private static Boolean isProfileIdentified = false;
	private static CMSProfile__c profileIdentified;
	
	global static List<User> getUserAssingToContact(String username) {
		return [
				SELECT Contact.Registration_Approved__c,
						Contact.CMSProfile__c,
						Contact.CMSProfile__r.Force_com_Authorization_and_Registration__c,
						Contact.CMSProfile__r.Name__c,
						Contact.CMSProfile__r.Default_Home_Page__r.Name__c,
						Contact.Company_Name__c
				FROM User
				WHERE Username = :username
				LIMIT 1
		];
	}
	
	global static Contact getLoggedInContact() {
		if (isContactIdentified) {
			return contactIdentified;
		}
		isContactIdentified = true;
		
		LoggerCMS.addMessage('CMSHelper', 'getLoggedInContact', 'getLoggedInContact');
		
		Id contactId = Login.getLoggedInContact();
		
		LoggerCMS.addMessage('CMSHelper', 'GetLoggedInContact', 'contactId=' + contactId);
		
		/* Edited by Aliaksandr Satskou, 07/24/2013 (case #000202021) */
		contactIdentified = (contactId != null)
				? Database.query(
						' SELECT ' + ContactFieldsUtil.getAllFieldsString() + //' Name ' +
								' FROM Contact WHERE Id=\'' + contactId + '\'')
				: new Contact();
		
		return contactIdentified;
	}
	
	global static PageReference login(String username, String password) {
		LoggerCMS.addMessage('CMSHelper', 'login', '');
		
		List<Contact> contactList = new List<Contact>();
		Contact currentContact;
		List<User> users = new List<User>();
		User customerPortalUser = new User();
		
		users = CMSHelper.getUserAssingToContact(username);
		
		if (users.size() > 0) {
			customerPortalUser = users[0];
		}
		if (customerPortalUser != null &&
				customerPortalUser.Contact.CMSProfile__r.Force_com_Authorization_and_Registration__c) {
			
			try {
				
				Site.login(username, password, '');
			} catch (Exception e) {
				System.debug(LoggingLevel.ERROR, e.getMessage());
			}
			
			if (!ApexPages.hasMessages(ApexPages.Severity.ERROR)) {
				if (decryptString(customerPortalUser.Contact.Company_Name__c) != password) {
					customerPortalUser.Contact.Company_Name__c = encryptString(password);
					contactList.add(customerPortalUser.Contact);
					update customerPortalUser.Contact;
				
				} else {
					contactList.add(customerPortalUser.Contact);
				}
			} else {
				return null;
			}
		} else {
			contactList = [
					SELECT Registration_Approved__c, CMSProfile__c, Company_Name__c,
							CMSProfile__r.Name__c,
							CMSProfile__r.Default_Home_Page__r.Name__c
					FROM Contact c
					WHERE UserName__c = :username.toLowerCase()
					LIMIT 1
			];
		}
		
		if (contactList.size() > 0 && decryptString(contactList[0].Company_Name__c) == password) {
			currentContact = contactList[0];
		}
		
		if (currentContact == null) {
			LoggerCMS.addErrorMessage(System.Label.Your_login_attempt_has_failed);
			return null;
		}
		
		if (!currentContact.Registration_Approved__c) {
			LoggerCMS.addErrorMessage(System.Label.Your_are_not_approved_by_your_manager_to_login);
		}
		
		if (currentContact.CMSProfile__r.Name__c == null) {
			LoggerCMS.addErrorMessage(System.Label.No_profile_is_assigned_to_this_contact);
		}
		
		if (!ApexPages.hasMessages(ApexPages.Severity.ERROR)) {
			if (customerPortalUser != null &&
					customerPortalUser.Contact.CMSProfile__r.Force_com_Authorization_and_Registration__c) {
				
				PageReference redirect = Site.login(username, password, CMSHelper.doLogin
						(currentContact, currentContact.CMSProfile__r.Name__c,
								currentContact.CMSProfile__r.Default_Home_Page__r.Name__c).getUrl());
				
				if (!ApexPages.hasMessages(ApexPages.Severity.ERROR)) {
					return redirect;
				}
			} else {
				return CMSHelper.doLogin(currentContact, currentContact.CMSProfile__r.Name__c,
						currentContact.CMSProfile__r.Default_Home_Page__r.Name__c);
			}
		}
		return null;
	}
	
	public static PageReference doLogin(Contact contact, String profileName, String homePage) {
		LoggerCMS.addMessage('CMSHelper', 'doLogin', 'contact=' + contact);
		
		Session__c session = new Session__c();
		String sessionId = String.valueOf(Math.random()).substring(2, 16);
		
		session.Session_For__c = contact.Id;
		session.User_Agent__c = Login.getUserAgent();
		session.IP_Address__c = Login.getIP();
		session.SessionId__c = sessionId;
		insert session;
		
		Cookie sessionCookie = new Cookie(sessionId, sessionId, null, -1, false);
		ApexPages.currentPage().setCookies(new List<Cookie> {sessionCookie});
		
		LoggerCMS.addMessage('CMSHelper', 'doLogin', 'session=' + session);
		
		PageReference pg;
		String startUrl = ApexPages.currentPage().getParameters().get('startUrl');
		
		LoggerCMS.addMessage('CMSHelper', 'doLogin', 'startUrl=' + startUrl);
		
		if (startUrl != null && startUrl.length() > 0 && startUrl != 'null') {
			pg = new PageReference(startUrl.replace('~', '&'));
		} else {
			pg = new PageReference('/apex/FCMS__CMSLayout');
			pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
			pg.getParameters().put('page', homePage);
		}
		
		pg.getParameters().put('p', profileName);
		pg.getParameters().put('sessionId', session.SessionId__c);
		pg.setRedirect(true);
		
		LoggerCMS.addMessage('CMSHelper', 'doLogin', 'pg=' + pg);
		
		return pg;
	}
	
	global static Boolean isCurrentUserForceDotComLicensed() {
		CMSProfile__c profile = getProfile();
		System.debug('!!!!!!!!!!!!!profile=' + profile);
		return profile.Force_com_Authorization_and_Registration__c;
	}
	
	// Gets css styles for current profile
	global static String getCSSStyles() {
		
		LoggerCMS.addMessage('CMSHelper', 'GetCSSStyles', '');
		
		CMSProfile__c profile = getProfile();
		
		LoggerCMS.addMessage('CMSHelper', 'GetCSSStyles', 'profile=' + profile);
		
		List<ApexPage> pageList = [
				SELECT Markup
				FROM ApexPage
				WHERE Name = :profile.CMSCss__c
				LIMIT 1
		];
		
		LoggerCMS.addMessage('CMSHelper', 'GetCSSStyles', 'pageList=' + pageList);
		
		if (pageList.size() > 0) {
			String cmsCssCustom = pageList[0].Markup;
			
			if (cmsCssCustom != null) {
				Integer startIndex = cmsCssCustom.indexOf('>');
				cmsCssCustom = cmsCssCustom.substring(startIndex + 1);
				cmsCssCustom = cmsCssCustom.replace('</apex:page>', '');
				
				LoggerCMS.addMessage('CMSHelper', 'GetCSSStyles', 'cmsCssCustom found');
				
				return cmsCssCustom.replace('background:#414141;', '');
			}
		}
		
		LoggerCMS.addMessage('CMSHelper', 'GetCSSStyles', 'cmsCssCustom not found');
		
		return '';
	}
	
	public static CMSProfile__c getProfile() {
		if (isProfileIdentified) {
			return profileIdentified;
		}
		isProfileIdentified = true;
		
		String profileName = ApexPages.currentPage().getParameters().get('p');
		if (profileName == null) {
			return null;
		}
		
		LoggerCMS.addMessage('CMSHelper', 'getProfile', 'profileName=' + profileName);
		
		Contact contact = getLoggedInContact();
		
		String wherePart = (contact.Id != null)
				? ' Id = \'' + contact.CMSProfile__c + '\'' : ' Name__c = \'' + profileName + '\'';
		
		profileIdentified = Database.query(
				' SELECT Force_com_Authorization_and_Registration__c, CMSCss__c, Name__c ' +
						' FROM CMSProfile__c ' +
						' WHERE ' + wherePart +
						' LIMIT 1 ');
		
		LoggerCMS.addMessage('CMSHelper', 'getProfile', 'profileIdentified=' + profileIdentified);
		
		return profileIdentified;
	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
	
	public static void sendResetPasswordEmail(Id contactId) {
		Contact contact = [
				SELECT Job_Site__c
				FROM Contact
				WHERE Id = :contactId
		];
		
		String resetPasswordUrl =
				ByPassLogin__c.getOrgDefaults().ByPassLogin__c +
						'FCMS__CMSBypassLogin?id=' + encryptString(contactId) +
						'&jobSite=' + contact.Job_Site__c +
						'&page=Update+Profile';
		
		EmailTemplate template = [
				SELECT Subject, Body
				FROM EmailTemplate
				WHERE DeveloperName = 'Changed_Password'
		];
		
		template.Body = template.Body.replace('{!Change_Password_URL}', resetPasswordUrl);
		
		String[] subjectAndBody = mergeFields(
				new List<ContentToMerge> {
						new ContentToMerge(
								template.Subject,
								null, UserInfo.getUserId(),
								null, contactId, null),
						new ContentToMerge(
								template.Body,
								null, UserInfo.getUserId(),
								null, contactId, null)
				});
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setSubject(subjectAndBody[0]);
		email.setPlainTextBody(subjectAndBody[1]);
		email.setTargetObjectId(contactId); // Contact/Lead/User Id
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
	}
	
	public static MetadataUtil mdUser = new MetadataUtil(new User());
	public static MetadataUtil mdContact = new MetadataUtil(new Contact());
	
	public static List<String> mergeFields(List<ContentToMerge> contentsToMerge) {
		List<String> textWithFormulasList = new List<String>();
		List<Id> currentUserIds = new List<Id>();
		List<Id> contactIds = new List<Id>();
		List<User> jobApplicantOwners = new List<User>();
		
		for (ContentToMerge contentToMerge : contentsToMerge) {
			textWithFormulasList.add(contentToMerge.textWithFormulas);
			currentUserIds.add(contentToMerge.currentUserId);
			contactIds.add(contentToMerge.contactId);
			jobApplicantOwners.add(contentToMerge.jobApplicantOwner);
		}
		
		textWithFormulasList = mergeFields(
				textWithFormulasList, mdUser, currentUserIds, null);
		textWithFormulasList = mergeFields(
				textWithFormulasList, mdContact, contactIds, null);
		
		return textWithFormulasList;
	}
	
	private static List<String> mergeFields(
			List<String> textWithFormulasListOriginal, MetadataUtil md,
			List<Id> recordIds, List<User> owners) {
		
		System.debug(LoggingLevel.ERROR, ':::::mergeFields: textWithFormulasListOriginal = ' + textWithFormulasListOriginal);
		
		if (recordIds == null || recordIds.size() == 0) {
			return textWithFormulasListOriginal;
		}
		
		String objectName = md.getObjectName();
		
		Map<String, String> fields = new Map<String, String>();
		for (String text : textWithFormulasListOriginal) {
			Matcher matcher = Pattern.compile('\\{!(.+?)\\}').matcher(text);
			
			System.debug(LoggingLevel.ERROR, ':::::mergeFields: objectName = ' + objectName);
			
			while (matcher.find()) {
				String fieldName = matcher.group(1);
				
				System.debug(LoggingLevel.ERROR, ':::::mergeFields: fieldName = ' + fieldName);
				
				if (fieldName.startsWith(objectName + '.') && fields.get(fieldName) == null) {
					fields.put(fieldName, fieldName.replace(objectName + '.', ''));
				}
			}
		}
		
		System.debug(LoggingLevel.ERROR, ':::::mergeFields: fields = ' + fields);
		
		if (fields.size() == 0) {
			return textWithFormulasListOriginal;
		}
		
		Set<String> whatNameSet = new Set<String> {'ID', 'NAME'};
		
		for (String fieldAPIName : fields.values()) {
			if (fieldAPIName != 'OwnerFullName'
					&& fieldAPIName != 'OwnerTitle'
					&& fieldAPIName != 'OwnerEmail'
					&& fieldAPIName != 'OwnerPhone'
					&& fieldAPIName != 'OwnerFirstName'
					&& fieldAPIName != 'OwnerLastName'
					&& fieldAPIName != 'Link') {
				
				whatNameSet.add(fieldAPIName.toUpperCase());
			}
		}
		
		System.debug(LoggingLevel.ERROR, ':::::mergeFields: whatNameSet = ' + whatNameSet);
		
		String fieldsCommaDelimited = String.join(new List<String>(whatNameSet), ', ');
		
		String query = '' +
				' SELECT ' + fieldsCommaDelimited +
				' FROM ' + objectName +
				' WHERE Id IN :recordIds';
		
		System.debug(LoggingLevel.ERROR, ':::::mergeFields: query = ' + query);
		
		Map<Id, SObject> recordWithValuesMap = new Map<Id, SObject>(Database.query(query));
		
		List<String> textWithFormulasListResult = new List<String>();
		
		for (Integer i = 0; i < recordIds.size(); i++) {
			Id recordId = recordIds[i];
			String textWithFormulas = textWithFormulasListOriginal[i];
			
			if (recordId != null) {
				User owner = owners != null ? owners[i] : null;
				
				SObject recordWithValues = recordWithValuesMap.get(recordId);
				
				for (String fieldName : fields.keySet()) {
					String fieldLocalName = fields.get(fieldName);
					
					Object fieldValue = '';
					if (fieldLocalName == 'OwnerFullName') {
						fieldValue = owner == null
								? ''
								: (owner.Name != null ? owner.Name : '');
					} else if (fieldLocalName == 'OwnerTitle') {
						fieldValue = owner == null
								? ''
								: (owner.Title != null ? owner.Title : '');
					} else if (fieldLocalName == 'OwnerEmail') {
						fieldValue = owner == null
								? ''
								: (owner.Email != null ? owner.Email : '');
					} else if (fieldLocalName == 'OwnerPhone') {
						fieldValue = owner == null
								? ''
								: (owner.Phone != null ? owner.Phone : '');
					} else if (fieldLocalName == 'OwnerFirstName') {
						fieldValue = owner == null
								? ''
								: (owner.FirstName != null ? owner.FirstName : '');
					} else if (fieldLocalName == 'OwnerLastName') {
						fieldValue = owner == null
								? ''
								: (owner.LastName != null ? owner.LastName : '');
					} else if (fieldLocalName == 'Link') {
						fieldValue = '<a href="' +
								System.Url.getSalesforceBaseUrl().toExternalForm() + '/' +
								recordWithValues.get('Id') + '">' +
								recordWithValues.get('Name') + '</a>';
					} else {
						fieldValue = (Object)recordWithValues.get(fieldLocalName);
						fieldValue = md.getFieldValue(fieldLocalName, fieldValue);
						fieldValue = (fieldValue == null) ? '' : fieldValue;
					}
					
					textWithFormulas = textWithFormulas.replace(
							'{!' + fieldName + '}', '' + fieldValue);
				}
			}
			
			textWithFormulasListResult.add(textWithFormulas);
		}
		
		return textWithFormulasListResult;
	}
	
	public with sharing class ContentToMerge {
		public String textWithFormulas;
		public Id jobAppId;
		public Id currentUserId;
		public Id jobId;
		public Id contactId;
		public User jobApplicantOwner;
		
		public ContentToMerge(String textWithFormulas,
				Id jobAppId, Id currentUserId,
				Id jobId, Id contactId,
				User jobApplicantOwner) {
			this.textWithFormulas = textWithFormulas;
			this.jobAppId = jobAppId;
			this.currentUserId = currentUserId;
			this.jobId = jobId;
			this.contactId = contactId;
			this.jobApplicantOwner = jobApplicantOwner;
		}
	}
	
	// Unused
	global static CustomSettingsComponent__c getCustomSettings() {
		return null;
	}
}