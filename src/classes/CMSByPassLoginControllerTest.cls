@isTest
private class CMSByPassLoginControllerTest {

  private static testMethod void loginTest() {
    
    Account accObj = new Account(Name = 'Avankia');
    
    insert accObj;

    Page__c pageObj = new Page__c();

    CMSProfile__c cmsProfileObj = new CMSProfile__c(Name__c = 'TestProfile',
                            Profile_Account__c = accObj.id, CMSCss__c = 'testCMS',
                            User_Registration__c = true,
                            Force_com_Authorization_and_Registration__c = true);
    insert cmsProfileObj;
    
    Contact conObj = new Contact(LastName = 'Shashish',
								        UserName__c = 'shashish.k@targetrecruit.net',
		    Company_Name__c ='uI1pZrEcalmTgcHQ0sZ6tstWh2pnQuY8URy4rlUUtrU=',
								        CMSProfile__c = cmsProfileObj.id,
								        Registration_Approved__c = true);
        
    insert conObj;
    
    Apexpages.currentPage().getParameters().put('Id', conObj.id);
    CMSByPassLoginController byPassLoginControllerObj = new CMSByPassLoginController();
    byPassLoginControllerObj.login();

  }

}