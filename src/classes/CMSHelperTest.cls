@isTest
private class CMSHelperTest {
    @isTest
    private static void myUnitTest() {
        String sessionId = '62486944141904';
        ApexPages.currentPage().getParameters().put('sessionId', sessionId);

        Page__c testFCMSPage = new Page__c(Name = 'testFCMSPage',
                                                                    Name__c = 'testFCMSName');
        insert testFCMSPage;

        CMSProfile__c testFCMSProfile = new CMSProfile__c(Name__c = 'testFCMSName',
                                                                    Name = 'testFCMSProfile');
        insert testFCMSProfile;

        Account testAcc = new Account(Name = 'Test');
        insert testAcc;

        Contact testCon = new Contact(LastName = 'Test', AccountId = testAcc.Id,
                                Email = 'test@test.test', UserName__c = 'test@test.test',
		        Company_Name__c = 'uI1pZrEcalmTgcHQ0sZ6tstWh2pnQuY8URy4rlUUtrU=');
        insert testCon;

        Session__c session = new Session__c(Is_Valid__c = true,
                                        Session_For__c = testCon.Id, SessionId__c = sessionId);
        insert session;
        
        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('page',testFCMSPage.Name__c);
        pg.getParameters().put('sessionId',session.SessionId__c);
        pg.getParameters().put('p',testFCMSProfile.Name__c);
        
        Test.setCurrentPageReference(pg);

        CMSHelper controller = new CMSHelper();

        CMSHelper.doLogin(testCon, testFCMSProfile.Name,
                            'testPage');

        CMSHelper.getUserAssingToContact(String.valueOf(UserInfo.getUserName()));

        CMSHelper.getLoggedInContact();

        CMSHelper.getCSSStyles();
        CMSHelper.getProfile();
        CMSHelper.isCurrentUserForceDotComLicensed();

        CMSHelper.getCustomSettings();

        CMSHelper.login(testCon.UserName__c, testCon.Company_Name__c);
    }
}