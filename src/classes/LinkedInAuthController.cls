/**
* @author Rakesh Rangu
* @date 07/01/2014
* @description An apex class used to login from LinkedIn
*/
public without sharing class LinkedInAuthController extends SocialLoginHelper {
    
    public Map<String,String> profileInfo = new Map<String,String>();
    public Map<String,String> userInfo = new Map<String,String>();

    //Initializes from LinkedInAuthPage
    public PageReference linkedInLogin(){

        String code = ApexPages.currentPage().getParameters().get('code');
        String state = ApexPages.currentPage().getParameters().get('state');
        String email='';
        String accessTokenUrl = '';
        String profileUrl = '';
        String emailUrl = '';
        
        if (code != null) {
            System.debug('code'+code);
    
            
            if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
            	accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code='+code+                                    
                             '&client_id='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Client_Id__c+
                             '&client_secret='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Client_Secret__c+
                             '&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__LinkedInAuth?jobSite='+ApexPages.currentPage().getParameters().get('jobSite');
            } else {
            	accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code='+code+                                    
                             '&client_id='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Client_Id__c+
                             '&client_secret='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Client_Secret__c+
                             '&redirect_uri='+ConfigHelper.getConfigDataSet().FCMS__LinkedIn_Redirect_Uri__c+'FCMS__LinkedInAuth';
            	
            }
    
            profileUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,industry)?';
    
            emailUrl = 'https://api.linkedin.com/v1/people/~/email-address?';
    
            String tokenResp = '';
    
            tokenResp = postDataNew(accessTokenUrl,'POST');
    
            if (tokenResp!='error') {
                Response resp;
                resp = (Response)JSON.deserialize(tokenResp,Response.class);
                //Fetching Profile Information
                if (resp.access_token!=null) {
    
                    profileUrl += 'oauth2_access_token='+resp.access_token;
                    String profileResp = postDataNew(profileUrl,'GET');
                    System.debug('..profile Response..'+profileResp);
                    if (profileResp!=null && profileResp!='error') {
                            profileInfo = parseProfileInfo(profileResp);
                            System.debug('...First Name...'+profileInfo.get('first_name'));
                    }
    
                    //Fetching Email Information
                    emailUrl += 'oauth2_access_token='+resp.access_token;
                    System.debug('Email URL..'+emailUrl);
                    String emailResp = postDataNew(emailUrl,'GET');
                    String emailId = '';
                    if (emailResp!=null && emailResp!='error') {
                            emailId = parseEmailInfo(emailResp);
                    }
    
                    userInfo.put('email',emailId);
                    userInfo.put('firstName',profileInfo.get('first_name'));
                    userInfo.put('lastName',profileInfo.get('last_name'));
                    userInfo.put('id',profileInfo.get('linkedIn_id'));
    
                    System.debug('User Info:'+userInfo);
                    PageReference pg = registerSocialUser(userInfo,'LinkedIn');
                    System.debug('page reference:'+pg);
                    return pg;
    
                }
            } else {
                    return null;
            }
        } else {
            if (ApexPages.currentPage().getParameters().get('error') == 'access_denied') {
            	System.debug('jobSite Parameter:'+ApexPages.currentPage().getParameters().get('jobSite'));
               FCMS__CMSProfile__c p = [SELECT FCMS__Name__c
                FROM CMSProfile__c
                WHERE User_Registration__c = true AND Order__c = 1
                ORDER BY Order__c
                LIMIT 1];
                
                PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
                pg.getParameters().put('page', 'CMSSiteLogin');
                pg.getParameters().put('p', p.FCMS__Name__c);
                
                if(ApexPages.currentPage().getParameters().get('jobSite') != null) {
                	pg.getParameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
                }
                
                pg.setRedirect(true);
                return pg;
            }
        } 
        return null;
    }

    /**
    * @author Rakesh Rangu
    * @date 07/01/2014
    * @description Used to parse profile information
    */
    public Map<String,String> parseProfileInfo(String resp){
        Map<String,String> profileDetails = new Map<String,String>();
        try{
                DOM.Document doc = new DOM.Document();
                doc.load(resp);
                DOM.XMLNode root = doc.getRootElement();

                if (root.getNodeType() == DOM.XMLNodeType.ELEMENT && root.getName() == 'person'){
                        System.debug('...First Name...'+root.getChildElement('first-name', null).getText().trim());
                        profileDetails.put('first_name', root.getChildElement('first-name', null).getText().trim());
                        profileDetails.put('last_name',root.getChildElement('last-name', null).getText().trim());
                        profileDetails.put('linkedIn_id',root.getChildElement('id', null).getText().trim());
                }

                return profileDetails;
            }catch(XMLException e){

                return profileDetails;
            }
        }

    /**
    * @author Rakesh Rangu
    * @date 07/01/2014
    * @description Used to parse Email information
    */
    public String parseEmailInfo(String resp){
        String email='';
        try{
                DOM.Document doc = new DOM.Document();
                doc.load(resp);
                DOM.Xmlnode root = doc.getRootElement();
                if(root.getNodeType() == DOM.Xmlnodetype.ELEMENT && root.getName() == 'email-address'){
                    System.debug('...Email...'+root.getText().trim());
                    email = root.getText().trim();
                }
                return email;
        }catch(XMLException e){
            return email;
        }
    }

        
    /**
    * @author Rakesh Rangu
    * @date 07/01/2014
    * @description Response class for fetching access token
    */
    public class Response {

        public String expires_in { get; set; }
        public String access_token { get; set; }
    }

}