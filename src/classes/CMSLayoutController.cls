public without sharing class CMSLayoutController {
    private Page__c page;

    public String cmsCssCustom { get; set; }
    public String metaText { get; set; }
    public String pageTitle { get; set; }
    public String layoutWidth { get; set; }

    private String subject = System.Label.CMSLayout_Page_Error;

    public void init() {
        try {
            
            LoggerCMS.addMessage('init', 'CMSLayoutController', '');
            
            CMSProfile__c profile = CMSHelper.getProfile();
            System.debug('!!!!!!!!!!!!!!!!!!!!!!!!!userId=' + UserInfo.getUserId());
            LoggerCMS.addMessage('init', 'CMSLayoutController', 'profile=' + profile);

            cmsCssCustom = CMSHelper.getCSSStyles();

            LoggerCMS.addMessage('init', 'CMSLayoutController', 'cmsCssCustom=' + cmsCssCustom);

            List<Page__c> pages = [
                    SELECT Name, Meta_Text__c, Name__c,
                        Authentication_Required__c, Active__c
                    FROM Page__c
                    WHERE Active__c = true
                    AND Name__c = :ApexPages.currentPage().getParameters().get('page')];

            if (pages.size() == 0) {
                LoggerCMS.addErrorMessage(System.Label.This_URL_does_not_exist);

                return;
            }

            page = pages[0];

            List<Permission__c> permissions;
            if (page.Authentication_Required__c) {
                
                permissions = [
                        SELECT Id
                        FROM Permission__c
                        WHERE Page__r.Name__c = :page.Name__c
                            AND CMSProfile__r.Name__c = :profile.Name__c];


            LoggerCMS.addMessage
                    ('CMSLayoutController', 'CMSLayoutController', 'permissions=' + permissions);
            }
            
            if (permissions != null && permissions.size() == 0) {
                
                LoggerCMS.addErrorMessage
                                (System.Label.You_have_not_sufficient_rights_to_access_this_page);
            }

            LoggerCMS.addMessage('CMSLayoutController', 'CMSLayoutController', 'page=' + page);

            metaText = page != null ? page.Meta_Text__c : '';
            pageTitle = page != null ? page.Name : '';

            setLayoutWidth(profile);
        } catch (Exception e) {
            LoggerCMS.catchBlockMethod(e, subject);
        }
    }

    private void setLayoutWidth(CMSProfile__c profile) {
        
        LoggerCMS.addMessage('CMSLayoutController', 'setLayoutWidth', '');

        String sections = '';
        

        List<Permission__c> blockList = [
                SELECT Block__r.Section__c
                FROM Permission__c
                WHERE CMSProfile__c = :profile.id
                AND Block__r.Active__c = true
                AND (Block__r.Page__r.Name__c = ''
                OR Block__r.Page__r.Name__c = :ApexPages.currentPage().getParameters().get('page'))
                ORDER BY Block__r.Weight__c];

        LoggerCMS.addMessage('CMSLayoutController', 'setLayoutWidth', 'blockList=' + blockList);

        for (Permission__c block : blockList) {
            sections = sections + ';' + block.Block__r.Section__c;
        }
        
        LoggerCMS.addMessage('CMSLayoutController', 'setLayoutWidth', 'sections=' + sections);

        if (sections.length() > 0 && sections.contains('left') && sections.contains('right'))
            layoutWidth = 'sidebars';
        else if (sections.length() > 0 && sections.contains('left'))
            layoutWidth = 'sidebar-left';
        else if (sections.length()>0 && sections.contains('right'))
            layoutWidth = 'sidebar-right';
        else if (sections.length()>0 && !sections.contains('right') && !sections.contains('left'))
            layoutWidth = 'cms-default';
        else if (sections.length()>0 && sections.contains('right') &&
                sections.contains('left') && sections.contains('content'))
            layoutWidth = 'sidebars';

        LoggerCMS.addMessage('CMSLayoutController', 'setLayoutWidth', 'layoutWidth=' + layoutWidth);
    }

    public PageReference redirectIfNotLoggedIn() {
        
        PageReference pg = null;

        try {

            init();

            if (!ApexPages.hasMessages(ApexPages.Severity.ERROR)) {
                
                LoggerCMS.addMessage('CMSLayoutController', 'redirectIfNotLoggedIn', '');
                
                LoggerCMS.addMessage('CMSLayoutController', 'redirectIfNotLoggedIn', 'getLoggedInContact');

                Id contactId = Login.getLoggedInContact();

                LoggerCMS.addMessage('CMSLayoutController', 'redirectIfNotLoggedIn',
                        'contactId=' + contactId);

                if (page.Authentication_Required__c) {
                    if (contactId == null) {
                        
                        String startUrl = ApexPages.currentPage().getUrl().replace('&', '~');

                        LoggerCMS.addMessage
                        ('CMSLayoutController', 'redirectIfNotLoggedIn', 'startUrl=' + startUrl);

                        pg = new PageReference('/FCMS__CMSLayout');
                        pg.getParameters().put('page', 'CMSSiteLogin');
                        pg.getParameters().put('startUrl', startUrl);
                        pg.getParameters().put
                        ('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
                        pg.getParameters().put
                        ('p', ApexPages.currentPage().getParameters().get('p'));
                        pg.setRedirect(true);
                    } else {
                        
                        Contact cont = [
                                SELECT Registration_Approved__c,
                                CMSProfile__r.Need_Registration_Approval__c,
                                CMSProfile__r.Approval_Message_Page__r.Name__c
                                FROM Contact
                                WHERE Id = :contactId];

                        LoggerCMS.addMessage('CMSLayoutController', 'redirectIfNotLoggedIn',
                                'cont=' + cont);

                        if (cont.CMSProfile__r.Need_Registration_Approval__c &&
                            !cont.Registration_Approved__c) {
                            pg = new PageReference(
                                    '/FCMS__CMSLayout?page=' +
                                    cont.CMSProfile__r.Approval_Message_Page__r.Name__c);
                            pg.setRedirect(true);
                        }
                    }
                }
            }

            LoggerCMS.sendLog(subject);
        } catch (Exception e) {
            LoggerCMS.catchBlockMethod(e, subject);
        }
        return pg;
    }

    public static testmethod void tstCMSLayoutController() {
        List<Account> accList = [SELECT id FROM Account WHERE Name = 'Candidate Pool2'];
        if (accList.size()>0) {
            delete accList;
        }
        Account account = new Account();
        account.Name = 'Candidate Pool2';
        insert account;
        List<CMSProfile__c> profileList = [SELECT id FROM CMSProfile__c];
        delete profileList;
        CMSProfile__c cmsProfile = new CMSProfile__c();
        cmsProfile.Name__c = 'CMSDefaultProfile';
        cmsProfile.Profile_Account__c = account.id;
        cmsProfile.CMSCss__c = 'CMSCssCustom';
        cmsProfile.User_Registration__c = true;
        insert cmsProfile;

        Contact contact = new Contact();
        contact.FirstName = 'Ramesh';
        contact.LastName = 'Dhull';
        contact.UserName__c = 'dhull.ramesh@gmail.com';
        contact.Email = 'dhull.ramesh@gmail.com';
        contact.Company_Name__c = 'uI1pZrEcalmTgcHQ0sZ6tstWh2pnQuY8URy4rlUUtrU=';
        contact.CMSProfile__c = cmsProfile.id;
        insert contact;
        Page__c page = new Page__c();
        page.Name = 'OnlineJobSearch';
        page.Name__c = 'OnlineJobSearch';
        page.Active__c = true;
        insert page;
        Menu__c menu = new Menu__c();
        menu.Name = 'OnlineJobSearch';
        menu.Name__c = 'OnlineJobSearch';
        menu.Active__c = true;
        menu.Order__c = 1;
        menu.Page__c=page.id;
        insert menu;
        Menu_Component__c menuComponent = new Menu_Component__c();
        menuComponent.Name__c = 'OnlineJobSearch';
        menuComponent.Component_Menus__c = 'OnlineJobSearch';
        menuComponent.Active__c = true;
        menuComponent.Display_Type__c = 'horizontal';
        menuComponent.Override_Class__c = 'parent-menu';
        insert menuComponent;
        for (Integer i=0;i<5;i++) {
            
            Block__c block = new Block__c();
            block.Name__c = 'OnlineJobSearch'+i;
            block.Page__c = page.id;
            block.Width__c = '400px';
            block.Height__c = '400px';
            block.Weight__c = 1;
            block.Override_Class__c ='';
            block.Section__c = 'right';
            block.Type__c = System.Label.Menu_Component;
            block.Menu_Component_Name__c = menuComponent.Name__c;
            block.IFrame_URL__c = '';
            block.Content__c = '<h1>Hello How are you?</h1>';
            block.Active__c =true;
            insert block;
            Permission__c permission = new Permission__c();
            permission.CMSProfile__c = cmsProfile.id;
            permission.Page__c = page.id;
            permission.Block__c = block.id;
            insert permission;
            Block__c block1 = new Block__c();
            block1.Name__c = 'OnlineJobSearchIFRAME'+i;
            block1.Page__c = page.id;
            block1.Width__c = '400px';
            block1.Height__c = '400px';
            block1.Weight__c = 1;
            block1.Override_Class__c ='';
            block1.Section__c = 'right';
            block1.Type__c = System.Label.IFRAME;
            block1.Menu_Component_Name__c = menuComponent.Name__c;
            block1.IFrame_URL__c = '';
            block1.Content__c = '<h1>Hello How are you?</h1>';
            block1.Active__c =true;
            insert block1;
            Permission__c permission1 = new Permission__c();
            permission1.CMSProfile__c = cmsProfile.id;
            permission1.Page__c = page.id;
            permission1.Block__c = block1.id;
            insert permission1;
            Block__c block2 = new Block__c();
            block2.Name__c = 'OnlineJobSearchFlash'+i;
            block2.Page__c = page.id;
            block2.Width__c = '400px';
            block2.Height__c = '400px';
            block2.Weight__c = 1;
            block2.Override_Class__c ='';
            block2.Section__c = 'right';
            block2.Type__c = System.Label.Flash;
            block2.Menu_Component_Name__c = menuComponent.Name__c;
            block2.IFrame_URL__c = '';
            block2.Content__c = '<h1>Hello How are you?</h1>';
            block2.Active__c =true;
            insert block2;
            Permission__c permission2 = new Permission__c();
            permission2.CMSProfile__c = cmsProfile.id;
            permission2.Page__c = page.id;
            permission2.Block__c = block2.id;
            insert permission2;
            Block__c block3 = new Block__c();
            block3.Name__c = 'OnlineJobSearchMODULE'+i;
            block3.Page__c = page.id;
            block3.Width__c = '400px';
            block3.Height__c = '400px';
            block3.Weight__c = 1;
            block3.Override_Class__c ='';
            block3.Section__c = 'right';
            block3.Type__c = System.Label.MODULE;
            block3.Menu_Component_Name__c = menuComponent.Name__c;
            block3.IFrame_URL__c = '';
            block3.Content__c = '<h1>Hello How are you?</h1>';
            block3.Active__c =true;
            insert block3;
            Permission__c permission3 = new Permission__c();
            permission3.CMSProfile__c = cmsProfile.id;
            permission3.Page__c = page.id;
            permission3.Block__c = block3.id;
            insert permission3;
            Block__c block4 = new Block__c();
            block4.Name__c = 'OnlineJobSearchHTML'+i;
            block4.Page__c = page.id;
            block4.Width__c = '400px';
            block4.Height__c = '400px';
            block4.Weight__c = 1;
            block4.Override_Class__c ='';
            block4.Section__c = 'right';
            block4.Type__c = System.Label.HTMLCMS;
            block4.Menu_Component_Name__c = menuComponent.Name__c;
            block4.IFrame_URL__c = '';
            block4.Content__c = '<h1>Hello How are you?</h1>';
            block4.Active__c =true;
            insert block4;
            Permission__c permission4 = new Permission__c();
            permission4.CMSProfile__c = cmsProfile.id;
            permission4.Page__c = page.id;
            permission4.Block__c = block4.id;
            insert permission4;
            Block__c block5 = new Block__c();
            block5.Name__c = 'OnlineJobSearchExisting Block'+i;
            block5.Page__c = page.id;
            block5.Width__c = '400px';
            block5.Height__c = '400px';
            block5.Weight__c = 1;
            block5.Override_Class__c ='';
            block5.Section__c = 'right';
            block5.Type__c = System.Label.Existing_Block;
            if (i==0) {
                block5.Existing_Block_Name__c = block.Name__c;
            }
            if (i==1) {
                block5.Existing_Block_Name__c = block1.Name__c;
            }
            if (i==2) {
                block5.Existing_Block_Name__c = block2.Name__c;
            }
            if (i==3) {
                block5.Existing_Block_Name__c = block3.Name__c;
            }
            if (i==4) {
                block5.Existing_Block_Name__c = block4.Name__c;
            }
            if (i==5) {
                block5.Existing_Block_Name__c = block5.Name__c;
            }
            
            block5.Menu_Component_Name__c = menuComponent.Name__c;
            block5.IFrame_URL__c = '';
            block5.Content__c = '<h1>Hello How are you?</h1>';
            block5.Active__c =true;
            insert block5;
            Permission__c permission5 = new Permission__c();
            permission5.CMSProfile__c = cmsProfile.id;
            permission5.Page__c = page.id;
            permission5.Block__c = block5.id;
            insert permission5;

        }
        Permission__c permission6 = new Permission__c();
        permission6.CMSProfile__c = cmsProfile.id;
        permission6.Page__c = page.id;
        insert permission6;
        Permission__c permission7 = new Permission__c();
        permission7.CMSProfile__c = cmsProfile.id;
        permission7.Menu__c = menu.id;
        insert permission7;

        Map<String,String> headerMap = System.currentPageReference().getHeaders();
        String sessionString;
        sessionString = String.valueOf(Math.random());
        sessionString = sessionString.substring(2,16);
        Session__c session = new Session__c();
        session.Session_For__c = contact.Id;
        session.User_Agent__c=headerMap.get('User-Agent');
        session.IP_Address__c=headerMap.get('X-Salesforce-SIP');
        session.SessionId__c = sessionString;
        insert session;

        Test.startTest();

        CMSBlockBean cmsBlockBean = new CMSBlockBean();
        List<cmsBlockBean.CMSMenuBean> menuBean = new List<cmsBlockBean.CMSMenuBean>();
        cmsBlockBean.setCmsMenuBeanList(menuBean);
        cmsBlockBean.getCmsMenuBeanList();

        PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
        pg.getParameters().put('page',page.Name__c);
        pg.getParameters().put('sessionId',session.SessionId__c);
        pg.getParameters().put('p',cmsProfile.Name__c);
        Test.setCurrentPageReference(pg);
        CMSLayoutController cmsLayoutController = new CMSLayoutController();
        cmsLayoutController.redirectIfNotLoggedIn();

        CMSBlocksBuilder.cmsBlockBuilderMethod('header');
        page.Authentication_Required__c = true;
        update page;
        CMSBlocksBuilder.cmsBlockBuilderMethod('header');
        PageReference pg1 = new PageReference('/apex/FCMS__CMSLayout');
        pg1.getParameters().put('page',page.Name__c);
        pg.getParameters().put('p',cmsProfile.Name__c);

        Test.setCurrentPageReference(pg1);
        CMSLayoutController cmsLayoutController1 = new CMSLayoutController();
        cmsLayoutController1.redirectIfNotLoggedIn();
        page.Authentication_Required__c = false;
        update page;
        cmsLayoutController1.redirectIfNotLoggedIn();
        Test.stopTest();
    }
}