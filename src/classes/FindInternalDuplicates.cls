global without sharing virtual class FindInternalDuplicates {
	global virtual List<Contact> getListOfInternalDuplicates(Contact contact) {
		LoggerCMS.addMessage('FindInternalDuplicates', 'getListOfInternalDuplicates', '');
		
		FCMS__CMSProfile__c profile = [
				SELECT FCMS__Name__c, FCMS__User_Record_Type__c
				FROM FCMS__CMSProfile__c
				WHERE Id = :contact.FCMS__CMSProfile__c];
		
		String csValue = FCMS__CustomSettingsComponent__c.getValues('Default').FCMS__User_RecordTypes_for_Duplicate_Check__c;
		if (csValue != null && csValue != '') {
			LoggerCMS.addMessage('FindInternalDuplicates', 'getListOfInternalDuplicates', '>>>>>>>>>>>csValue = ' + csValue);
			List<String> userRecordTypesList = csValue.trim().split(',');
			if (userRecordTypesList.size() > 0) {
				Set<String> userRecordTypesSet = new Set<String>(userRecordTypesList);
				LoggerCMS.addMessage('FindInternalDuplicates', 'getListOfInternalDuplicates', '1>>>>>>>>>>>userRecordTypesSet = ' + userRecordTypesSet);
				List<Id> requiredRecordTypeIdList = MetadataUtil.getRecordTypeIdList(userRecordTypesSet);
				if (userRecordTypesSet.contains(profile.FCMS__User_Record_Type__c)) {
					LoggerCMS.addMessage('FindInternalDuplicates', 'getListOfInternalDuplicates', '3>>>>>>>>>>>userRecordTypesSet = ' + userRecordTypesSet);
					return [
							SELECT FCMS__CMSProfile__c
							FROM Contact
							WHERE Email = :contact.Email
								AND FCMS__UserName__c = ''
								AND Company_Name__c = ''
								AND RecordTypeId IN : requiredRecordTypeIdList
							LIMIT 1];
				}
			}
		}
		return new List<Contact>();
	}
}