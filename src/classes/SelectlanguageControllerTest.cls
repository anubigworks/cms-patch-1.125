@isTest
private class SelectlanguageControllerTest {

	private static testMethod void correctLoad() {
		Cookie v_selectLanguage = new Cookie('selectLanguage', 'en-US', null, -1, false);
		ApexPages.currentPage().setCookies(new List<Cookie> { v_selectLanguage });

		SelectlanguageController controller = new SelectlanguageController();

	}

	private static testMethod void standardLoad() {
		SelectlanguageController controller = new SelectlanguageController();

		List<SelectOption> languages = controller.languages;
	}

	private static testMethod void changeLanguage() {
		Cookie v_selectLanguage = new Cookie('selectLanguage', 'en-US', null, -1, false);
		ApexPages.currentPage().setCookies(new List<Cookie> { v_selectLanguage });

		SelectlanguageController controller = new SelectlanguageController();

		System.assertEquals(null, controller.init());

		controller.selectLanguage = 'es';

	}
}