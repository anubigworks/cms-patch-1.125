public class BlockController {
	private Block__c block = getBlk();
	private List<Block__c> blockList;
	private Boolean showHideContent = true;
	public Boolean showHideTextContent { get; set; }
	public Boolean showHideMenuComponent { get; set; }
	public Boolean showHideMODULE { get; set; }
	public Boolean showHideIFRAME { get;set; }
	public Boolean showHideAutoHeightIFRAME { get; set; }
	public Boolean showHideExistingBlock { get; set; }
	public Boolean showHideFlash { get; set; }
	public String existingBlockName;
	public String menuComponentName;
	public String moduleName;

	public BlockController() {
		setBlockType();
	}

	public Boolean getShowHideContent() {
		return showHideContent;
	}

	public String getExistingBlockName() {
		existingBlockName=block.Existing_Block_Name__c;
		return existingBlockName;
	}

	public void setExistingBlockName(String existingBlockName) {
		this.existingBlockName = existingBlockName;
		block.Existing_Block_Name__c = existingBlockName;
	}

	public String getMenuComponentName() {
		menuComponentName=block.Menu_Component_Name__c;
		return menuComponentName;
	}

	public void setMenuComponentName(String menuComponentName) {
		this.menuComponentName = menuComponentName;
		block.Menu_Component_Name__c=menuComponentName;
	}

	public String getModuleName() {
		moduleName=block.Module_Name__c;
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName=moduleName;
		block.Module_Name__c=moduleName;
	}

	public List<SelectOption> getExistingBlockItems() {

		   List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(System.Label.NoneCMS,System.Label.NoneCMS));

		List<Block__c> existingBlockList = [
				SELECT Name, Name__c
				FROM Block__c
				WHERE Active__c = true
				ORDER BY Name__c];

		for(Block__c block:existingBlockList) {
			options.add(new SelectOption(block.Name__c,block.Name__c));
		}

		return options;
	}

	public List<SelectOption> getModuleItems() {

		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(System.Label.NoneCMS, System.Label.NoneCMS));

		List<ApexPage> pageList = [SELECT a.Name,a.NamespacePrefix, a.Id FROM ApexPage a];

		for(ApexPage page : pageList) {

			if (page.NamespacePrefix!=null && page.NamespacePrefix.length()>0) {
				options.add(new SelectOption(page.NamespacePrefix+'__'+
						page.Name,page.NamespacePrefix+'__'+page.Name));
			} else {
				options.add(new SelectOption(page.Name,page.Name));
			}
		}
		return options;
	}
	public List<SelectOption> getItems() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(System.Label.NoneCMS, System.Label.NoneCMS));

		List<Menu_Component__c> menuComponentList = [
				SELECT id,Name,Name__c
				FROM Menu_Component__c
				WHERE Active__c = true];

		for(Menu_Component__c menuComponent : menuComponentList) {
			options.add(new SelectOption(menuComponent.Name__c,menuComponent.Name__c));
		}

		return options;
	}

	public Block__c getBlock() {
		return block;
	}
	public void setBlock(Block__c block) {
		this.block = block;
	}

	public Block__c getBlk() {
		String blockId = ApexPages.currentPage().getParameters().get('id');
		String pageId = ApexPages.currentPage().getParameters().get('pageId');
		if (blockId!=null) {

			blockList = [
					SELECT Name,b.Auto_Height_Iframe_Code__c,b.TextContent__c,
					b.Add_Parameters_From_Parent_Window__c, b.Flash_Play__c,
					b.Flash_Loop__c,b.FlashVars__c,b.Existing_Block_Name__c,b.Width__c,
					b.Weight__c, b.Type__c,b.Menu_Component_Name__c,b.Section__c, b.Page__c,
					b.OwnerId,b.Override_Class__c,b.Name__c, b.Module_Name__c,b.IFrame_URL__c,
					b.Height__c, b.Content__c, b.Authentication_Required__c, b.Active__c
					FROM Block__c b
					WHERE b.id = :blockId];
		}

		if (blockList!=null && blockList.size()>0) {
			block = blockList[0];

		} else {

			block = new Block__c();
			block.Type__c = System.Label.HTMLCMS;

			if (pageId!=null && pageId.length()>0) {
				block.Page__c=pageId;
			}
		}
		System.debug('*****ramesh*****getBlk()*****'+block);
		return block;
	}
	public void setBlockType() {

		if(block.Type__c == System.Label.IFRAME) {

			System.debug('****ramesh****changeBlockType()**IFRAME*'+block.Type__c);
			showHideIFRAME = true;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = false;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.Flash) {

					showHideIFRAME = true;
					showHideMODULE = false;
					showHideMenuComponent = false;
					showHideContent = false;
					showHideExistingBlock = false;
					showHideFlash = true;
					showHideTextContent = false;
					showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.MODULE) {

			System.debug('****ramesh****changeBlockType()**MODULE*****'+block.Type__c);
			showHideIFRAME = false;
			showHideMODULE = true;
			showHideMenuComponent = false;
			showHideContent = false;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;

		} else if(block.Type__c == System.Label.HTMLCMS) {

			System.debug('****ramesh****changeBlockType()**HTML*****'+block.Type__c);
			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = true;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.Menu_Component) {

			System.debug('****ramesh****changeBlockType()**Menu Component*****'+block.Type__c);
			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = true;
			showHideContent = false;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.Existing_Block) {

			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = false;
			showHideExistingBlock = true;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.Text) {

			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = false;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = true;
			showHideAutoHeightIFRAME = false;

		} else if (block.Type__c == System.Label.Auto_Height_Iframe) {

			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = false;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = true;

		} else {

			System.debug('****ramesh****changeBlockType()******None****'+block.Type__c);
			showHideIFRAME = false;
			showHideMODULE = false;
			showHideMenuComponent = false;
			showHideContent = true;
			showHideExistingBlock = false;
			showHideFlash = false;
			showHideTextContent = false;
			showHideAutoHeightIFRAME = false;
		}
	}

	private void addErrorMessage(String message) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
			ApexPages.addMessage(msg);
	}

	public PageReference save() {

		System.debug('*****ramesh*****block.Page__c***'+block.Page__c);

		List<Block__c> blkList = [
				SELECT id
				FROM Block__c
				WHERE Name__c != :block.Name__c
				AND  Section__c=:block.Section__c
				AND Weight__c = :block.Weight__c
				AND (Page__c = :block.Page__c
				OR Page__c = null)];

		if (blkList.size()>0) {
			addErrorMessage(System.Label.Combination_of_selected_Section);
			return null;
		}

		String ckeditor = ApexPages.currentPage().getParameters().get('editor1');

		if (ckeditor!=null) {
			block.Content__c = ckeditor;
		}

		String textContent = ApexPages.currentPage().getParameters().get('textContent');

		if (textContent!=null) {
			block.TextContent__c = textContent;
		}

		String autoHeightIframeCode =
				ApexPages.currentPage().getParameters().get('autoHeightIframeCode');

		if (autoHeightIframeCode!=null) {
					block.Auto_Height_Iframe_Code__c = autoHeightIframeCode;
		}

		System.debug('*******ramesh****ckeditor***'+ckeditor);
		if (block.id==null) {
			if (block.Type__c == System.Label.IFRAME && (block.IFrame_URL__c !=null &&
					block.IFrame_URL__c.length()>0)) {

			} else if (block.Type__c == System.Label.MODULE && (block.Module_Name__c !=null &&
							block.Module_Name__c!= System.Label.NoneCMS)) {

			} else if (block.Type__c == System.Label.Menu_Component &&
							(block.Menu_Component_Name__c !=null &&
							block.Menu_Component_Name__c != System.Label.NoneCMS)) {

			} else if (block.Type__c == System.Label.HTMLCMS) {

			} else if (block.Type__c == System.Label.Text) {

			} else if (block.Type__c == System.Label.Flash && (block.IFrame_URL__c !=null &&
							   block.IFrame_URL__c.length()>0)) {

			} else if (block.Type__c == System.Label.Existing_Block &&
							  (block.Existing_Block_Name__c !=null &&
							block.Existing_Block_Name__c != System.Label.NoneCMS)){

			} else if (block.Type__c == null) {

			} else if (block.Type__c == System.Label.Auto_Height_Iframe &&
							  (block.Auto_Height_Iframe_Code__c !=null &&
							   block.Auto_Height_Iframe_Code__c.length()>0)) {

			} else {
				addErrorMessage(System.Label.Please_select_or_enter_some_value);
				return null;
			}

			insert block;

		} else {
			if (block.Type__c == System.Label.IFRAME
						&& (block.IFrame_URL__c !=null && block.IFrame_URL__c.length()>0)) {

			} else if (block.Type__c == System.Label.MODULE
							&& (block.Module_Name__c !=null
							&& block.Module_Name__c!=System.Label.NoneCMS)){

			} else if (block.Type__c == System.Label.Menu_Component
							&& (block.Menu_Component_Name__c !=null
							&& block.Menu_Component_Name__c !=System.Label.NoneCMS)){

			} else if (block.Type__c == System.Label.HTMLCMS) {

			} else if (block.Type__c == System.Label.Text) {

			} else if (block.Type__c == System.Label.Flash
							&& (block.IFrame_URL__c !=null && block.IFrame_URL__c.length()>0)){

			} else if (block.Type__c == System.Label.Existing_Block &&
						(block.Existing_Block_Name__c !=null &&
						block.Existing_Block_Name__c !=System.Label.NoneCMS)) {

			} else if (block.Type__c == null) {

			} else if (block.Type__c == System.Label.Auto_Height_Iframe &&
						(block.Auto_Height_Iframe_Code__c !=null
						&& block.Auto_Height_Iframe_Code__c.length()>0)) {

			} else {
				addErrorMessage(System.Label.Please_select_or_enter_some_value);
				return null;
			}

			update block;
		}

		PageReference pg = new PageReference('/'+block.id);
		return pg;
	}

	public PageReference saveAndNew() {

		List<Block__c> blkList = [
				SELECT id
				FROM Block__c
				WHERE Name__c != :block.Name__c
				AND  Section__c=:block.Section__c
				AND Weight__c = :block.Weight__c
				AND (Page__c = :block.Page__c
				OR Page__c = '')];

		if (blkList.size()>0) {
			addErrorMessage(System.Label.Combination_of_selected_Section);
			return null;
		}

		String ckeditor = ApexPages.currentPage().getParameters().get('editor1');

		if (ckeditor!=null) {
			block.Content__c = ckeditor;
		}

		String textContent = ApexPages.currentPage().getParameters().get('textContent');

		if (textContent!=null) {
			block.TextContent__c = textContent;
		}

		String autoHeightIframeCode =
				ApexPages.currentPage().getParameters().get('autoHeightIframeCode');

		if (autoHeightIframeCode!=null) {
			block.Auto_Height_Iframe_Code__c = autoHeightIframeCode;
		}
		System.debug('*******ramesh****ckeditor***'+ckeditor);
		if (block.id==null) {

			if (block.Type__c == System.Label.IFRAME
					&& (block.IFrame_URL__c !=null
					&& block.IFrame_URL__c.length()>0)) {

			} else if (block.Type__c == System.Label.MODULE
							&& (block.Module_Name__c !=null
							&& block.Module_Name__c!=System.Label.NoneCMS)) {

			} else if (block.Type__c == System.Label.Menu_Component &&
							(block.Menu_Component_Name__c !=null
							&& block.Menu_Component_Name__c !=System.Label.NoneCMS)) {

			} else if (block.Type__c == System.Label.HTMLCMS) {

			} else if (block.Type__c == System.Label.Text) {

			} else if (block.Type__c == System.Label.Flash &&
							(block.IFrame_URL__c !=null
							&& block.IFrame_URL__c.length()>0)) {

			} else if (block.Type__c == System.Label.Existing_Block &&
							(block.Existing_Block_Name__c !=null
							&& block.Existing_Block_Name__c !=System.Label.NoneCMS)) {

			} else if (block.Type__c == null) {

			} else if (block.Type__c == System.Label.Auto_Height_Iframe &&
							(block.Auto_Height_Iframe_Code__c !=null
							&& block.Auto_Height_Iframe_Code__c.length()>0)) {

			} else {

				addErrorMessage(System.Label.Please_select_or_enter_some_value);
				return null;

			}
			insert block;

		} else {
				if (block.Type__c == System.Label.IFRAME &&
						(block.IFrame_URL__c !=null
						&& block.IFrame_URL__c.length()>0)) {

				} else if (block.Type__c == System.Label.MODULE
							&& (block.Module_Name__c !=null
							&& block.Module_Name__c!=System.Label.NoneCMS)) {

				} else if (block.Type__c == System.Label.Menu_Component &&
								(block.Menu_Component_Name__c !=null
								&& block.Menu_Component_Name__c !=System.Label.NoneCMS)) {

				} else if (block.Type__c == System.Label.HTMLCMS) {

				} else if (block.Type__c == System.Label.Text){

				} else if (block.Type__c == System.Label.Flash &&
								(block.IFrame_URL__c !=null &&
								block.IFrame_URL__c.length()>0)) {

				} else if (block.Type__c == System.Label.Existing_Block &&
								(block.Existing_Block_Name__c !=null &&
								block.Existing_Block_Name__c !=System.Label.NoneCMS)) {

				} else if (block.Type__c == null) {

				} else if (block.Type__c == System.Label.Auto_Height_Iframe &&
								(block.Auto_Height_Iframe_Code__c !=null &&
								block.Auto_Height_Iframe_Code__c.length()>0)) {

			} else {
				addErrorMessage(System.Label.Please_select_or_enter_some_value);
				return null;
			}

			update block;
		}

		PageReference pg = new PageReference('/apex/FCMS__New_Edit_Block');
		pg.setRedirect(true);
		return pg;
	}

	public PageReference cancel () {
		Schema.DescribeSObjectResult D = Block__c.sObjectType.getDescribe();
		PageReference pg;

		if (block.id==null) {
			pg= new PageReference('/'+d.getKeyPrefix());
		} else {
			pg= new PageReference('/'+block.id);
		}
		return pg;
	}

	public PageReference changeBlockType() {

		if (block.Name__c =='.') {
			block.Name__c = '';
		}

		String ckeditor = ApexPages.currentPage().getParameters().get('editor1');

		if (ckeditor!=null) {
			block.Content__c = ckeditor;
		}

		setBlockType();
		return null;
	}

	public static testmethod void tstBlockController() {

		BlockController blockController = new BlockController();
		blockController.setMenuComponentName('TestComponent');
		blockController.getMenuComponentName();
		blockController.setModuleName('FCMS__Home');
		blockController.getModuleName();
		blockController.getShowHideContent();
		blockController.getExistingBlockItems();
		blockController.getModuleItems();
		blockController.getItems();
		blockController.block.Name__c = 'Home0';
		blockController.block.Weight__c = 0.0;
		blockController.setBlock(blockController.block);
		blockController.getBlock();
		blockController.changeBlockType();
		blockController.block.Type__c = System.Label.IFRAME;
		blockController.setBlockType();
		blockController.block.Type__c = System.Label.Flash;
		blockController.setBlockType();
		blockController.block.Type__c = System.Label.MODULE;
		blockController.setBlockType();
		blockController.block.Type__c = System.Label.Menu_Component;
		blockController.setBlockType();
		blockController.block.Type__c = System.Label.Existing_Block;
		blockController.setBlockType();
		blockController.block.Type__c = System.Label.NoneCMS;
		blockController.setBlockType();
		blockController.save();
		blockController.setExistingBlockName(System.Label.MODULE);
		blockController.getExistingBlockName();
		BlockController blockController1 = new BlockController();
		blockController1.block.Name__c = 'Home';
		blockController1.block.Weight__c = 0.0;
		blockController1.saveAndNew();
		Page__c page = new Page__c();
		page.Name = 'OnlineJobSearch';
		page.Name__c = 'OnlineJobSearch';
		page.Active__c = true;
		insert page;
		PageReference pg = new PageReference('/apex/FCMS__New_Edit_Block');
		pg.getParameters().put('id',blockController.block.id);
		pg.getParameters().put('pageId',page.id);
		Test.setCurrentPageReference(pg);
		BlockController blockController2 = new BlockController();
		blockController2.block.Name__c = 'Home1';
		blockController2.block.Weight__c = 0.0;
		blockController2.save();
		BlockController blockController3 = new BlockController();
		blockController3.block.Name__c = 'Home2';
		blockController3.block.Weight__c = 0.0;
		blockController2.saveAndNew();
		blockController2.cancel();
	}
}