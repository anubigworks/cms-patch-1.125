public without sharing class SetPasswordController {
	public String password { get; set; }
	
	private Id contactId;
	
	public SetPasswordController(ApexPages.StandardController controller) {
		contactId = controller.getId();
		
		Contact contact = [
				SELECT FCMS__Company_Name__c
				FROM Contact
				WHERE Id = :contactId
		];
		password = decryptString(contact.FCMS__Company_Name__c);
	}
	
	public PageReference save() {
		try {
			if (String.isBlank(password) || password.length() < 9) {
				throw new GeneralException('Password should be greater than 8 characters.');
			}
			
			Contact contact = [
					SELECT Id
					FROM Contact
					WHERE Id = :contactId
			];
			contact.FCMS__Company_Name__c = encryptString(password);
			update contact;
			
			return new PageReference('/' + contactId);
		} catch (Exception e) {
			ApexPages.addMessages(e);
			return null;
		}
	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}
	
	public with sharing class GeneralException extends Exception {}
}