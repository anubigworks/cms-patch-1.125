public without sharing class CMSForgotPasswordController {
	
	public String userName { get; set; }
	public String selectLanguage { get; set; }
	public String cmsCssCustom { get; set; }
	
	public CMSForgotPasswordController() {
		selectLanguage = UserInfo.getLocale();
		cmsCssCustom = CMSHelper.getCSSStyles();
	}
	
	public PageReference forgotPassword() {
		
		List<User> customerPortalUser = new List<User>();
		customerPortalUser = CMSHelper.getUserAssingToContact(username.tolowercase());
		
		if (customerPortalUser.size() > 0 && customerPortalUser[0].Contact.CMSProfile__r
				.Force_com_Authorization_and_Registration__c) {
			
			System.debug('::::::if ');
			
			boolean success = Site.forgotPassword(username.tolowerCase());
			
			System.debug('::::::if success=' + success);
			
			PageReference pr = new PageReference('/apex/FCMS__CMSLayout');
			pr.getparameters().put('page', 'CMSForgotPasswordConfirm');
			pr.setRedirect(true);
			
			System.debug(success);
			
			if (success) {
				return pr;
			} else {
				
				return null;
			}
		} else {
			
			if (!isValid()) {
				return null;
			}
			
			List<Contact> contacts;
			if (userName != null && userName != '') {
				
				System.debug(':::::::::::::;else');
				
				contacts = [
						SELECT Name,Company_Name__c,PortalEmailAlert__c,Email,UserName__c
						FROM Contact
						WHERE UserName__c = :userName.tolowerCase()
				];
				
				if (contacts.size() == 0) {
					addErrorMessage('User not found');
					return null;
				} else {
					
					try {
						CMSHelper.sendResetPasswordEmail(contacts[0].Id);
						
						PageReference pr = new PageReference('/apex/FCMS__CMSLayout');
						pr.getparameters().put('page','CMSForgotPasswordConfirm');
						pr.getparameters().put('jobSite',ApexPages.currentPage()
								.getParameters().get('jobSite'));
						pr.getparameters().put('p',ApexPages.currentPage().getParameters().get('p'));
						pr.setRedirect(true);
						return pr;
					} catch (Exception e) {
						
						addErrorMessage(System.Label.Error_double_dot + e);
						return null;
					}
				}
			}
		}
		return null;
	}
	
	public boolean isValid() {
		boolean valid = true;
		
		if (userName == '') {
			addErrorMessage(System.Label.User_name_is_required);
			valid = false;
		}
		
		return valid;
	}
	public PageReference backToHome() {
		
		PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
		pg.getparameters().put('page', 'CMSSiteLogin');
		pg.getparameters().put('jobSite', ApexPages.currentPage().getParameters().get('jobSite'));
		pg.getparameters().put('p', ApexPages.currentPage().getParameters().get('p'));
		pg.setRedirect(true);
		return pg;
	}
	
	private void addErrorMessage(String message) {
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
		ApexPages.addMessage(msg);
	}
	
	public static testmethod void tstCMSForgotPasswordController() {
		
		List<Account> accList = [
				SELECT id
				FROM Account
				WHERE Name = 'Candidate Pool2'
		];
		
		if (accList.size() > 0) {
			delete accList;
		}
		
		Account account = new Account();
		account.Name = 'Candidate Pool2';
		insert account;
		List<CMSProfile__c> profileList = [
				select id
				from CMSProfile__c
		];
		delete profileList;
		CMSProfile__c cmsProfile = new CMSProfile__c();
		cmsProfile.Name__c = 'CMSDefaultProfile';
		cmsProfile.Profile_Account__c = account.id;
		cmsProfile.CMSCss__c = 'CMSCssCustom';
		cmsProfile.User_Registration__c = true;
		insert cmsProfile;
		
		Contact contact = new Contact();
		contact.FirstName = 'Ramesh';
		contact.LastName = 'Dhull';
		contact.UserName__c = 'dhull.ramesh@gmail.com';
		contact.Email = 'dhull.ramesh@gmail.com';
		contact.Company_Name__c = 'uI1pZrEcalmTgcHQ0sZ6tstWh2pnQuY8URy4rlUUtrU=';
		contact.CMSProfile__c = cmsProfile.id;
		insert contact;
		
		PageReference pg = new PageReference('/apex/FCMS__CMSLayout');
		pg.getParameters().put('p', cmsProfile.Name__c);
		Test.setCurrentPageReference(pg);
		
		CMSForgotPasswordController forgotPasswordController = new CMSForgotPasswordController();
		forgotPasswordController.backToHome();
		forgotPasswordController.userName = '';
		forgotPasswordController.forgotPassword();
		forgotPasswordController.userName = contact.UserName__c;
		forgotPasswordController.forgotPassword();
		forgotPasswordController.userName = 'kdfgjfsdak';
		forgotPasswordController.forgotPassword();
		forgotPasswordController.addErrorMessage('hello');
	}
}