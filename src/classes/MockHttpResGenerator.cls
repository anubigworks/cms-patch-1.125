/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class MockHttpResGenerator implements HttpCalloutMock {

	static testMethod void myUnitTest() {
			// TO DO: implement unit test
	}
	global HTTPResponse respond(HTTPRequest req) {
					
		String fbUrl1 = 'https://graph.facebook.com/me?access_token=CAADeUxy7QLEBAIcIjROGuKa0B8FfzwGbU5F9ZA7ZA1iXwyq7kKPgUDBg6BNSAzKPtvDKanuObVnrYbOianhu8AI35jHP1x6X7KXGK5HM2dQntBFgDkhaKo2XB0K2DwKBgQGogDlc8CHwrg2aLOCZBe1SgUFVMyN2cccVpV3uaGbDYTY2FvBadTa85o0AGdypxq9JZCZAjIZB7hammgQzwN&fields=id,name,email,first_name,last_name';

		// Create a fake response
		HttpResponse res = new HttpResponse();
		//if(req.getEndpoint().contains('https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=AQQVbPlBn2jY1HDgDvBidD4aLfmxmotgRl0qUa8w6w0RuvUJqCUmoZW8xiUTCwnrfXev3GkRQLqlyfhm3mBfSdekjCQefvRcuod-LQY_OuVp5PLa11E&redirect_uri=https://fcms-developer-edition.ap1.force.com/FCMS__LinkedInAuth&client_id=75n4vp24esl36t&client_secret=IRh8mNKxg8RmMwDf')){
		if(req.getEndpoint().contains('https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=AQQVbPlBn2jY1HDgDvBidD4aLfmxmotgRl0qUa8w6w0RuvUJqCUmoZW8xiUTCwnrfXev3GkRQLqlyfhm3mBfSdekjCQefvRcuod-LQY_OuVp5PLa11E&client_id=75n4vp24esl36t&client_secret=IRh8mNKxg8RmMwDf&redirect_uri=https://fcms-developer-edition.ap1.force.com/FCMS__LinkedInAuth')){

			res.setHeader('Content-Type', 'application/json');
			res.setBody('{"expires_in":5183999,"access_token":"AQVoPqNDk5SJEiHVLSxYlbFog8Wa9V9NpwWh8FtUTKBQxhsQuThNhXmskeC5kkVfKJurUL4LfTA--ng7eON27ub4pN1eQp5Aj388kvOnue8fIpENjCU6dKuxYsYpp5CF-8oq8N82ZyqQ-iYNBYkGiH2-L5PoSPzC0xVjmyBca3dgh_Rqphs"}');
			res.setStatusCode(200);
			system.debug('First Call'+res.getBody());
			return res;
		}
		else if(req.getEndpoint().contains('https://api.linkedin.com/v1/people/~:(id,first-name,last-name,industry)?oauth2_access_token=AQVoPqNDk5SJEiHVLSxYlbFog8Wa9V9NpwWh8FtUTKBQxhsQuThNhXmskeC5kkVfKJurUL4LfTA--ng7eON27ub4pN1eQp5Aj388kvOnue8fIpENjCU6dKuxYsYpp5CF-8oq8N82ZyqQ-iYNBYkGiH2-L5PoSPzC0xVjmyBca3dgh_Rqphs')) {
			res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
				'<person>'+
					'<id>jxV2GxJLez</id>'+
					'<first-name>Rakesh</first-name>'+
					'<last-name>R</last-name>'+
					'<industry>Information Technology and Services</industry>'+
				'</person>');
			res.setStatusCode(200);
			system.debug('Second Call'+res.getBody());
			return res;

		}
		else if(req.getEndpoint().contains('https://api.linkedin.com/v1/people/~/email-address?oauth2_access_token=AQVoPqNDk5SJEiHVLSxYlbFog8Wa9V9NpwWh8FtUTKBQxhsQuThNhXmskeC5kkVfKJurUL4LfTA--ng7eON27ub4pN1eQp5Aj388kvOnue8fIpENjCU6dKuxYsYpp5CF-8oq8N82ZyqQ-iYNBYkGiH2-L5PoSPzC0xVjmyBca3dgh')) {
			res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
				'<email-address>jobposting123@hotmail.com</email-address>');
			res.setStatusCode(200);
			system.debug('Third call '+res.getBody());
			return res;

		}

		//else if(req.getEndpoint().contains('https://graph.facebook.com/oauth/access_token?client_id=244448545685681&redirect_uri=https://fcms-developer-edition.ap1.force.com/FCMS__FacebookAuth&client_secret=1ab96d48235197fd488923a5fcfc1519&code=')) {
		else if(req.getEndpoint().contains('https://graph.facebook.com/oauth/access_token?client_id=244448545685681&client_secret=1ab96d48235197fd488923a5fcfc1519&redirect_uri=https://fcms-developer-edition.ap1.force.com/FCMS__FacebookAuth&code=')) {
			System.debug('Im requesting for access token');
			res.setBody('access_token=CAADeUxy7QLEBAIcIjROGuKa0B8FfzwGbU5F9ZA7ZA1iXwyq7kKPgUDBg6BNSAzKPtvDKanuObVnrYbOianhu8AI35jHP1x6X7KXGK5HM2dQntBFgDkhaKo2XB0K2DwKBgQGogDlc8CHwrg2aLOCZBe1SgUFVMyN2cccVpV3uaGbDYTY2FvBadTa85o0AGdypxq9JZCZAjIZB7hammgQzwN&expires=5184000');
			res.setStatusCode(200);
			system.debug('First Call'+res.getBody());
			return res;
		}
		else if(req.getEndpoint().contains(fbUrl1)) {
			res.setHeader('Content-Type', 'application/json');
			res.setBody('{"id":"991858327498349","name":"Rakesh Rangu","email":"bunty.533@gmail.com","first_name":"Rakesh","last_name":"Rangu"}');
			res.setStatusCode(200);
			system.debug('First Call'+res.getBody());
			return res;
		}

		else if(req.getEndpoint().contains('https://accounts.google.com/o/oauth2/token')) {
			res.setHeader('Content-Type', 'application/json');
			res.setBody('{'+
					'"access_token" : "ya29.mQBbDvM1McxImYJpCvVRUGqDUUSfWBQnc6uINCqpIlu9_Xf-tVlzrd4T",'+
					'"token_type" : "Bearer",'+
					'"expires_in" : 3597,'+
					'"id_token" : "eyJhbGciOiJSUzI1NiIsImtpZCI6IjcxZWUyMzAzNzFkMTk2MzBiYzE3ZmI5MGNjZjIwYWU2MzJhZDhjZjgifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwic3ViIjoiMTAwODgyNzYxMDg4MTYxNjg2MTAyIiwiYXpwIjoiMTA3MDQ3NTc2MzU3NS1lYmQyczlqOG40MXFycWVpMDZkdTgyNjh1M2NhZXJoNy5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoicmFrZXNoLnJAdGFyZ2V0cmVjcnVpdC5uZXQiLCJhdF9oYXNoIjoicUctWVRsRjY4SUU5WVF4TmwzclpIZyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdWQiOiIxMDcwNDc1NzYzNTc1LWViZDJzOWo4bjQxcXJxZWkwNmR1ODI2OHUzY2Flcmg3LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiaGQiOiJ0YXJnZXRyZWNydWl0Lm5ldCIsImlhdCI6MTQxMjgzNzY2MSwiZXhwIjoxNDEyODQxNTYxfQ.L_7WABIMng-OaHmFsjjxTz0tRRe9Tr0o1T8dtTv8Y4nsn6aYtBTQtVUBVqDmbNpbF9iNtzbARqr9T6eavDeerW4-aBiqtgrlU7SNrcPs3lUOz3yLaffl_g-ImyX-5ZTPJ1daFtayhLtr7yzd7L6OtEhC6BAaxB4cjvMVFs-AH0E"'+
				'}');
			res.setStatusCode(200);
			system.debug('First Call'+res.getBody());
			return res;
		}
		else if(req.getEndpoint().contains('https://www.googleapis.com/oauth2/v1/userinfo?access_token=ya29.mQBbDvM1McxImYJpCvVRUGqDUUSfWBQnc6uINCqpIlu9_Xf-tVlzrd4T')) {
			res.setHeader('Content-Type', 'application/json');
			res.setBody('{'+
				'"id": "100882761088161686102",'+
				'"email": "rakesh.r@targetrecruit.net",'+
				'"verified_email": true,'+
				'"name": "Rakesh Rangu",'+
				'"given_name": "Rakesh",'+
				'"family_name": "Rangu",'+
				'"link": "https://plus.google.com/100882761088161686102",'+
				'"picture": "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",'+
				'"gender": "male",'+
				'"locale": "en-GB",'+
				'"hd": "targetrecruit.net"'+
				'}');
			res.setStatusCode(200);
			system.debug('First Call'+res.getBody());
			return res;
		}
		return res;
	}
}