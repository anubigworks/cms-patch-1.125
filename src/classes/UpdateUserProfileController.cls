global without sharing class UpdateUserProfileController {
	
	public String selectLanguage { get; set; }

	public Contact contact { get; set; }
	private Contact realContact;
	
	private String oldPassword;
	
	public Boolean isCurrentUserForceDotComLicensed { get; set; }

	public String cmsCssCustom { get; set; }

	public void init() {
		try {
			System.debug('!!!!!!!!!!!!!!userID=' + UserInfo.getUserId());
			Cookie v_selectLanguage = ApexPages.currentPage().getCookies().get('selectLanguage');

			selectLanguage = (v_selectLanguage != null && v_selectLanguage.getValue() != null)
					? v_selectLanguage.getValue() : 'en_US';

			realContact = CMSHelper.getLoggedInContact();
			contact = realContact.clone(false, false);

			isCurrentUserForceDotComLicensed = CMSHelper.isCurrentUserForceDotComLicensed();

			oldPassword = decryptString(realContact.Company_Name__c);
			System.debug('old passwordd:'+oldPassword);
			contact.Company_Name__c = null;

			cmsCssCustom = CMSHelper.getCSSStyles();
			
			/*String communityId = Network.getNetworkId();
			ConnectApi.FeedElementPage fep = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(
					communityId, ConnectApi.FeedType.News, 'me');*/
			
		} catch(Exception e) {
			LoggerCMS.catchBlockMethod(e, System.Label.Update_Profile_Page_Error);
		}
	}

	global PageReference reset() {
		PageReference pg = ApexPages.currentPage();
		pg.setRedirect(true);
		return pg;
	}

	global PageReference updateNow() {
		try {

			LoggerCMS.addMessage('UpdateUserProfileController', 'updateNow', 'contact.Password__c='
					+ contact.Company_Name__c);
			LoggerCMS.addMessage('UpdateUserProfileController', 'updateNow', 'confirmPassword='
					+ confirmPassword);
					
			System.debug('!!!!!!!!!!!!!!userID=' + UserInfo.getUserId());
			if (contact.Company_Name__c != null
					&& contact.Company_Name__c != ''
					&& contact.Company_Name__c.length() < 9) {

				LoggerCMS.addErrorMessage(ConfigHelperCMS.getConfigDataSet().PasswordError__c);
			}

			if (contact.Company_Name__c != confirmPassword) {
				LoggerCMS.addErrorMessage(ConfigHelperCMS.getConfigDataSet().ConfirmPasswordError__c);
			}
			
			contact.Company_Name__c = encryptString(contact.Company_Name__c);

				if (!ApexPages.hasMessages(ApexPages.Severity.Error)) {
					LoggerCMS.addMessage('UpdateUserProfileController', 'updateNow', 'updateRecords');
					
					if (isCurrentUserForceDotComLicensed){
						if (contact.Company_Name__c == null || contact.Company_Name__c == '')
							contact.Company_Name__c = encryptString(oldPassword);
						Site.changePassword(contact.Company_Name__c,contact.Company_Name__c,oldPassword);
					}
					
					updatePassword(contact,realContact);
				}



		} catch (Exception e) {
			LoggerCMS.catchBlockMethod(e, System.Label.Update_Profile_Page_Error);
		}

		return null;
	}
	
	private void updatePassword(Contact contact, Contact realContact) {

		ContactFieldsUtil.updateRecords(contact, realContact);

		if (realContact.Company_Name__c == null || realContact.Company_Name__c == '') {
			realContact.Company_Name__c = encryptString(oldPassword);
		}
		upsert realContact;

		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					ConfigHelperCMS.getConfigDataSet().ProfileUpdatedMsg__c));
	}
	
	public static String encryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String encrypted = EncodingUtil.base64Encode(
				Crypto.encryptWithManagedIV('AES128', getAesKey(), Blob.valueOf(str)));
		
		return encrypted;
	}
	
	public static String decryptString(String str) {
		if (String.isBlank(str)) {
			return str;
		}
		
		String decrypted = Crypto.decryptWithManagedIV(
				'AES128', getAesKey(), EncodingUtil.base64Decode(str)).toString();
		
		return decrypted;
	}
	
	private static Blob getAesKey() {
		return EncodingUtil.base64Decode('VEZ27Xlqw+mKLblpeAl93g==');
	}

	/*private static testmethod void tstUpdateUserProfileController() {
		delete [SELECT Id FROM Account WHERE Name = 'Candidate Pool2'];

		Account account = new Account();
		account.Name = 'Candidate Pool2';
		insert account;

		delete [SELECT Id FROM CMSProfile__c];

		CMSProfile__c cmsProfile = new CMSProfile__c();
		cmsProfile.Name__c = 'CMSDefaultProfile';
		cmsProfile.Profile_Account__c = account.id;
		cmsProfile.CMSCss__c = 'CMSCssCustom';
		cmsProfile.User_Registration__c = true;
		insert cmsProfile;

		delete [SELECT Id FROM Contact WHERE UserName__c = 'rajesh.d@avankia.com'];

		Contact contact = new Contact();
		contact.FirstName = 'Rajesh';
		contact.LastName = 'Dhull';
		contact.UserName__c = 'rajesh.d@avankia.com';
		contact.Email = 'rajesh.d@avankia.com';
		contact.CMSProfile__c = cmsProfile.id;
		contact.Registration_Approved__c = true;
		insert contact;

		Session__c session = new Session__c();
		session.Session_For__c = contact.Id;
		session.User_Agent__c = Login.getUserAgent();
		session.IP_Address__c = Login.getIP();
		session.SessionId__c = String.valueOf(Math.random()).substring(2,16);
		insert session;

		ApexPages.currentPage().getParameters().put('sessionId', session.SessionId__c);

		Test.startTest();

		UpdateUserProfileController cont = new UpdateUserProfileController();
		cont.init();
		cont.reset();
		cont.updateNow();

		cont.getCmsCssCustom();
		cont.getContactObject();
		cont.getContact();
		cont.setContact(contact);
		cont.getSuccessMessage();
		cont.setSuccessMessage(true);

		Test.stopTest();
	}*/

	// Unused code
	global transient Boolean successMessage = false;
	global transient String successMSG{ get; set; }
	global transient Boolean errorMessage{ get; set; }
	global String password{ get; set; }
	global String confirmPassword { get; set; }
	global String getCmsCssCustom(){ return null; }
	global Contact getContactObject(){ return null; }
	global Contact getContact(){ return null; }
	global void setContact(Contact contact){}
	global Boolean getSuccessMessage(){ return null; }
	global void setSuccessMessage(Boolean successMessage){}
}