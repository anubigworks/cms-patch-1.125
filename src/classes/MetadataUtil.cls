global without sharing class MetadataUtil {
	private SObject obj;
	global MetadataUtil (SObject obj) {
		this.obj = obj;
	}
	
	global Map<string, Schema.SObjectField> fieldMap {
		
		get {
			if (fieldMap == null)
				fieldMap = obj.getSObjectType().getDescribe().fields.getMap();
			return fieldMap;
		}
		set;
	}
	
	global String getAllFieldsString() {
		
		String result = '';
		System.Debug('asdasd');
		
		for (Schema.SObjectField field : fieldMap.values()) {
			System.Debug('asdasd=' + field.getDescribe().Name);
			result += ',' + field.getDescribe().Name;
		}
		
		return result.substring(1);
	}
	
	global void updateRecords(sObject source, sObject target) {
		
		for (string key : fieldMap.keySet()) {
			
			Object valueSource = source.get(key);
			Object valueTarget = target.get(key);
			if (valueSource != valueTarget) {
				try { target.put(key, valueSource); }
				catch(System.SObjectException ex) {}
			}
		}
	}
	
	/**
* @name getValue
* @param record sObject
* @param keyField String
* @return Object
*/
	global static Object getValue(sObject record, String keyField) {
		Object value;
		
		String[] keyLevels = keyField.split('\\.');
		
		for (Integer i = 0; i < keyLevels.size(); i++) {
			String keyLevel = keyLevels[i];
			
			if (record != null) {
				if (i < keyLevels.size() - 1) {
					record = (sObject)record.getSObject(keyLevel);
				} else {
					value = record.get(keyLevel);
				}
			}
		}
		
		return value;
	}
	
	global static List<Id> getRecordTypeIdList(Set<String> requiredRecordTypeNames) {
		List<Id> v_candidateRecordTypeIdList = new List<Id>();

		Map<String, Schema.RecordTypeInfo> recordTypes =
				Contact.sObjectType.getDescribe().getRecordTypeInfosByName();

		for (Schema.RecordTypeInfo recordType : recordTypes.values()) {
			String recordTypeName = recordType.getName();
			if (requiredRecordTypeNames.contains(recordTypeName))  {
				v_candidateRecordTypeIdList.add(recordType.getRecordTypeId());
			}
		}

		return v_candidateRecordTypeIdList;
	}
	
	public String getObjectName() {
		return  obj.getSObjectType().getDescribe().getName();
	}
	
	public Object getFieldValue(String name, Object value) {
		if (value == null) {
			return null;
		}
		
		return value;
	}

}